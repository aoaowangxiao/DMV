<%--
  Created by IntelliJ IDEA.
  User: Sky
  Date: 14-7-24
  Time: 下午2:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../includes/common.jsp" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="tab-pane active" id="MySQL">
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-link"></i></span>
        <input type="text" class="form-control" placeholder="服务器名称,例:127.0.0.1"
               id="myserver" value="localhost">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-screenshot"></i></span>
        <input type="text" class="form-control" placeholder="端口号,例:3306" id="myport"
               value="3306">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-user"></i></span>
        <input type="text" class="form-control" placeholder="用户名，例:root" id="myuser"
               value="root">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-lock"></i></span>
        <input type="password" class="form-control" placeholder="密码" id="mypassword"
               value="root">
    </div>
    <div  style="float: right;margin-top: 20px;" id="link">
        <button type="button" class="btn btn-primary" onclick="linkok()">连接</button>
    </div>
    <div style="display: none;float: right;margin-top: 20px;" style="display: none;" id="select-table">
        </button>
        <button type="button" class="btn btn-primary" onclick="connectDatabase()">确定</button>
    </div>
    <div id="select-databases"  style="float: left;margin-top: 25px;">
        <label>数据库选择</label>
        <select id="database" name="database"></select>
        <label>选择表</label>
        <select id="datatable" name="datatable"></select>
    </div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
    var flag = 0;

    function cancel() {
        parent.cancelp();
    }

    function linkok() {
        $.ajax({
            url: basePath + "jdbc/mysql",
            data: {
                url: $("#myserver").val().trim(),
                user: $("#myuser").val().trim(),
                port: $("#myport").val().trim(),
                password: $("#mypassword").val().trim()
            },
            type: "POST",
            dataType: "json",
            success: function (returnData) {
                document.getElementById("link").style.display = "none";
                document.getElementById("select-table").style.display = ""
                insertDatabase(returnData);
            },
            error: function () {
                alert("请求超时")
            }
        });
    }

    function insertDatabase(returnData) {
        var selectDatabase = $("#database");
        if (selectDatabase) {
            for (var i = 0; i < returnData.length; i++) {
                selectDatabase.append("<option value=" + returnData[i] + ">" + returnData[i] + "</option>");
            }
        }
    }

    function connectDatabase() {
        if (flag == 0) {
            $.ajax({
                url: basePath + "jdbc/mysqltables",
                data: {
                    url: $("#myserver").val().trim(),
                    user: $("#myuser").val().trim(),
                    port: $("#myport").val().trim(),
                    password: $("#mypassword").val().trim(),
                    databaseName: $("#database").val()
                },
                dataType: "json",
                type: "post",
                success: function (returnData) {
                    insertTable(returnData);
                    flag = 2;
                },
                error: function () {
                    alert("请求超时");
                }
            });
        } else {
            $.ajax({
                url: basePath + "jdbc/mysqlcopy",
                data: {
                    url: $("#myserver").val().trim(),
                    user: $("#myuser").val().trim(),
                    port: $("#myport").val().trim(),
                    password: $("#mypassword").val().trim(),
                    databaseName: $("#database").val(),
                    tableName: $("#datatable").val()
                },
                dataType: "json",
                type: "post",
                success: function (returnData) {
                    if (returnData == "0") {
                        parent.location.href = "data/table";
                    } else {
                        alert("出错");
                    }

                },
                error: doError
            });
        }
    }

    function insertTable(returnData) {
        var selectTable = $("#datatable");
        if (selectTable) {
            for (var i = 0; i < returnData.length; i++) {
                selectTable.append("<option value=" + returnData[i] + ">" + returnData[i] + "</option>");
            }
        }
    }
</script>