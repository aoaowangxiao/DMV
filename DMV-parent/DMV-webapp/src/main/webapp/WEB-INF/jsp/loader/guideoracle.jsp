<%--
  Created by IntelliJ IDEA.
  User: Sky
  Date: 14-7-24
  Time: 下午2:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../includes/common.jsp" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="tab-pane" id="Oracle">
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-link"></i></span>
        <input type="text" class="form-control" placeholder="服务器名称,例:127.0.0.1">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class=" icon-info-sign"></i></span>
        <input type="text" class="form-control" placeholder="服务名称,例:orcl">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-screenshot"></i></span>
        <input type="text" class="form-control" placeholder="端口号,例:3306">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-user"></i></span>
        <input type="text" class="form-control" placeholder="用户名，例:root">
    </div>
    </br>
    <div class="input-group">
        <span class="input-group-addon"><i class="icon-lock"></i></span>
        <input type="password" class="form-control" placeholder="密码">
    </div>
</div>
<div  style="float: right;margin-top: 20px;" id="link">
    <button type="button" class="btn btn-primary" onclick="linkok()">连接</button>
</div>
<div style="display: none;float: right;margin-top: 20px;" style="display: none;" id="select-table">
    </button>
    <button type="button" class="btn btn-primary" onclick="connectDatabase()">确定</button>
</div>
<div id="select-databases"  style="float: left;margin-top: 25px;">
    <label>数据库选择</label>
    <select id="database" name="database"></select>
    <label>选择表</label>
    <select id="datatable" name="datatable"></select>
</div>
</body>
</html>