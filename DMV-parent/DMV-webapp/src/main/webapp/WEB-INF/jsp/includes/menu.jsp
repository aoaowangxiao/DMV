<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="<c:url value="/img/logo/logo2.png"/>" alt="logo"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="loader/fileLoader">数据导入</a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">数据处理 <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="data/table">数据预览</a></li>
                    <li><a href="data/clean">数据清洗</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">可视化 <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="charts/scatter/showScatter">散点图</a></li>
                    <li><a href="charts/scatter/showScatter3D">三维散点图</a></li>
                    <li><a href="charts/bar/showBar">柱形图</a></li>
                    <li><a href="charts/line/showLine">折线图</a></li>
                    <%--<li><a href="charts/scatter/showScatter">条形图</a></li>--%>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">数据挖掘 <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="charts/scatter/kmeansScatter">聚类</a></li>
                    <li><a href="charts/tree/c45tree">分类</a></li>
                    <li><a href="charts/scatter/net">社交网络</a></li>
                    <li><a href="charts/line/aprioriParallel">关联规则</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>