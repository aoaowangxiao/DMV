<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<base href="<%=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()%><c:url value="/"/>"/>
<link rel="stylesheet" href="<c:url value="/dist/css/bootstrap.css"/>">
<link rel="stylesheet" href="<c:url value="/dist/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="/js/common/DataTables-1.10.0/media/css/jquery.dataTables.css"/>">
<link rel="stylesheet" href="<c:url value="/dist/css/buttons.css"/>">
<link rel="stylesheet" href="<c:url value="/css/global.css"/>">
<script src="<c:url value="/js/common/common.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/common/ccjcJS/ccjcJS.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/common/DataTables-1.10.0/media/js/jquery.js"/>"></script>
<script src="<c:url value="/js/common/DataTables-1.10.0/media/js/jquery.dataTables.js"/>"></script>
<script src="<c:url value="/dist/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/js/common/echarts/esl.js"/>" type="text/javascript"></script>
<!-- 返回顶部动画JS -->
<script type="text/javascript">
$(document).ready(function(){
        $(function () {
            //当点击跳转链接后，回到页面顶部位置
            $("#back-to-top").click(function(){
                $('body,html').animate({scrollTop:0},500);
                return false;
            });
        });
    });
</script>