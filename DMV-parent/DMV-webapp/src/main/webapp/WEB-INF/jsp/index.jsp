<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="./includes/common.jsp" %>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>数据挖掘可视化</title>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="/dmv/img/logo/logo2.png" alt="logo"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
</nav>
<div class="jumbotron" style="height: 45%;">
    <h1>您好!</h1>
    <p>欢迎使用DMV数据挖掘可视化软件，您可以通过两种方式进行可视化：</p>
</div>
<div class="page-container">
    <div class="row">
        <div class="col-md-6">
            <h2><i class="icon-book"></i> 向导模式</h2>

            <p>向导模式提供了基本数据挖掘的流程，您只要按照屏幕提示逐步操作即可完成数据挖掘可视化。如果您不是专业数据挖掘人员，或者这是您第一次使用本软件，那么选择向导模式可以让您轻松上手。 </p>

            <p><a class="button button-rounded  button-flat-primary" href="loader/guideFileLoader" role="button">向导模式</a></p>
        </div>
        <div class="col-md-6">
            <h2><i class="icon-flag"></i> 标准模式</h2>

            <p>标准模式适合有一定数据挖掘经验的用户，它提供了完整的选项，用户可以自定义挖掘顺序、调试参数和图形以最大化满足自己的需求。</p><br>

            <p><a class="button button-rounded  button-flat-primary" href="loader/fileLoader" role="button">标准模式</a></p>
        </div>
    </div>
</div>
<div class="page-footer" style="position: fixed;">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
