<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-6-7
  Time: 上午9:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="../includes/common.jsp" %>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>数据清洗</title>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="/dmv/img/logo/logo2.png" alt="logo"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
</nav>
<div class="jumbotron" style="height: 45%;">
    <h1>数据清洗</h1>
    <p>请选择是否进行数据清洗：</p>
</div>
<div class="page-container">
    <div class="row">
        <div class="col-md-6">
            <h2><i class=" icon-exclamation-sign"></i> 选择操作</h2><br/><br/>
            <div class="block-btn">
                <a onclick="cleanOK()" style="margin-right: 10px;" class="button button-rounded button-flat-primary button-large"><i class="icon-ok-sign"></i> 清洗</a>
                <a href="guide/visual" class="button button-rounded button-flat-action button-large"><i class="icon-circle-arrow-right"></i> 跳过</a>
            </div>
        </div>
        <div class="col-md-6">
            <h2><i class="icon-question-sign"></i> 帮助信息</h2><br/>
            <p>数据清洗指发现并纠正数据文件中可识别的错误的一道程序，包括检查数据一致性，处理无效值和缺失值等。</p>
            <p>本功能可清洗掉 <span class="label label-primary">缺失数据</span> <span class="label label-primary">离群数据</span></p>
        </div>
    </div>
</div>
<div class="page-footer" style="position: fixed;">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>

<script type="text/javascript">
    $(function () {
        $("#popComment").popover();
    });

    function cleanOK(){
        $.ajax({
            url: basePath + "guide/clean",
            type:"POST",
            dataType:"json",
            success:function(returnData) {
                alert("清洗成功！");
                window.location.href="guide/visual";
            },
            error:doError
        });

    }
</script>
</body>
</html>
