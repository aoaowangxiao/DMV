<%--@elvariable id="dataList" type="java"--%>
<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-5-16
  Time: 下午8:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="../includes/common.jsp" %>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>数据预览</title>
</head>
<body>
<%@ include file="../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        数据预览
        <small>本页以表格的形式显示所有数据</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="data/table">数据处理</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">数据预览</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-table"></i> 数据列表</h3>
        </div>
        <div class="panel-body">
            <div id="datatable">
            </div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
<script type="text/javascript">

    var dataSet = [];
    var columsSet = [];

    function initTable() {
        getAttr();
        getDataSet();
        $('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
        var table = $('#example').DataTable({
                    "data": dataSet,
                    "columns": columsSet
                }
        );
        $('.toggle-vis').on('click', function (e) {
            var thisObj = $(this);
            e.preventDefault();
            // Get the column API object
            var column = table.column($(this).attr('data-column'));
            // Toggle the visibility
            column.visible(!column.visible());
            thisObj.find("i").toggleClass("icon-check-empty")
        });
    }

    function getAttr() {
        var arr = [];
        $.ajax({
            url: basePath + "data/getAttr",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                columsSet = buildAttr(returnData);
            },
            error: doError
        });
        return arr;
    }

    function buildAttr(returnData) {
        var arr = [];
        for (var i = 0; i < returnData.length; i++) {
            arr[i] = { "sTitle": returnData[i] };
        }
        return arr;
    }

    function getDataSet() {
        $.ajax({
            url: basePath + "data/getdata",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                dataSet = buildDataSet(returnData);
            },
            error: doError
        });
    }

    function buildDataSet(returnData) {
        var dataSet = [];
        for (var i = 0; i < returnData.length; i++) {
            dataSet[i] = returnData[i];
        }
        return dataSet;
    }

    $(function () {
        initTable();
    })

</script>
</body>
</html>