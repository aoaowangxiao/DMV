<%--@elvariable id="dataList" type="java"--%>
<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-5-16
  Time: 下午8:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="../includes/common.jsp" %>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>数据清洗</title>
</head>
<body>
<%@ include file="../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        数据清洗
        <small>数据清洗选项在左侧工具栏</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="data/table">数据处理</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">数据清洗</li>
    </ol>
    <div class="panel panel-primary" style="float:left;width: 23%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-list-alt"></i> 定义变量属性</h3>
        </div>
        <div class="panel-body" id="clums">

        </div>
    </div>
    <div class="panel panel-primary" style="float:right;width: 75.5%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-table"></i> 数据列表</h3>
        </div>
        <div class="panel-body">
            <div id="demo">
            </div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:left;width: 23%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-table"></i> 缺失值处理</h3>
        </div>
        <div class="panel-body">
            <div class="radio" id="miss-del-label">
                <label>
                    <input type="radio" name="missValue" id="miss-del" value="miss-del" checked>全部删除
                </label>
            </div>
            <div class="radio">
                <label id="miss-avrage-label">
                    <input type="radio" name="missValue" id="miss-avrage" value="miss-avrage">平均数
                </label>
            </div>
            <div class="radio">
                <label id="miss-max-label">
                    <input type="radio" name="missValue" id="miss-max" value="miss-max">最大值
                </label>
            </div>
            <div class="radio">
                <label id="miss-min-label">
                    <input type="radio" name="missValue" id="miss-min" value="miss-min">最小值
                </label>
            </div>
            <div class="radio" id="miss-zero-label">
                <label>
                    <input type="radio" name="missValue" id="miss-zero" value="miss-zero">补零
                </label>
            </div>
            <a class="btn btn-primary" id="dis2" onclick="missValueDeal()" role="button">确定</a>
        </div>
    </div>
    <div class="panel panel-primary" id="departValueDeal" style="float:left;width: 23%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-table"></i> 离群值处理</h3>
        </div>
        <div class="panel-body">
            <div class="radio">
                <label>
                    <input type="radio" name="departValue" id="depart-del" value="depart-del" checked>删除
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="departValue" id="depart-edge" value="depart-edge">平均值
                </label>
            </div>
            <a class="btn btn-primary" id="dis3" onclick="departValueDeal()" role="button">确定</a>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>

</div>
<script type="text/javascript">

var dataSet = [];
var clm = buildAttr(getAttr());

function initTable() {

    getDataSet();
    $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
    var table = $('#example').DataTable({
        "data": dataSet,
        "columns": clm
    });

    $('.toggle-vis').on('click', function (e) {
        var thisObj = $(this);
        e.preventDefault();
        // Get the column API object
        var column = table.column($(this).attr('data-column'));
        // Toggle the visibility
        column.visible(!column.visible());
        thisObj.find("i").toggleClass("icon-check-empty")
    });

    //判断dataset中第一行，如果全为字符的话，直接隐藏离群值处理，并且禁用缺失值处理中，全部删除以外的选项
    var flag = false;
    for (var i = 0; i < dataSet[0].length; i++) {
        var isNum = !isNaN(dataSet[0][i]);
        flag += isNum;
    }
    ;
    if (!flag) {
        $("#departValueDeal").hide();
        document.getElementById("miss-avrage").disabled = true;
        document.getElementById("miss-max").disabled = true;
        document.getElementById("miss-min").disabled = true;
        document.getElementById("miss-zero").disabled = true;
    }
}


function getAttr() {
    var arr = [];
    $.ajax({
        url: basePath + "data/getAttr",
        dataType: "json",
        type: "POST",
        async: false,
        success: function (returnData) {
            arr = returnData;

        },
        error: doError
    });
    return arr;
}

function buildAttr(returnData) {
    var arr = [];
    for (var i = 0; i < returnData.length; i++) {
        arr[i] = { "sTitle": returnData[i] };
    }
    return arr;
}

function buildType(returnData) {
    var clum = $("#clums");
    for (var i = 0; i < returnData.length; i++) {
        var  chk=document.createElement("input");
        chk.id=returnData[i];
        chk.type="checkbox";
        chk.value="checkbox";
        clum.append(chk);
        clum.append(document.createTextNode(" "+returnData[i]));
        clum.append("<br/>");
        chk.setAttribute("checked","checked")
    }
    clum.append("<br/><a class='btn btn-primary' id='dis1' onclick='typeChange()' role='button'>确定</a>")
}

function getDataSet() {
    $.ajax({
        url: basePath + "data/getdata",
        dataType: "json",
        type: "POST",
        async: false,
        success: function (returnData) {
            dataSet = buildDataSet(returnData);
        },
        error: doError
    });
}

function buildDataSet(returnData) {
    var dataSet = [];
    for (var i = 0; i < returnData.length; i++) {
        dataSet[i] = returnData[i];
    }
    return dataSet;
}
// 定义变量属性
function typeChange() {
    var attr = getAttr();
    var deleteAttr = [];
    for (var i = 0; i < attr.length; i++) {
        var attrId = "#"+attr[i];
        var ck = document.getElementById(attr[i]);
        if(ck.checked){
            continue;
        }
        deleteAttr.push(attr[i]);
    }
    delclum(deleteAttr);  //干死他
    document.getElementById("dis1").className = "btn btn-primary disabled";
}

function delclum(deleteAttr) {
    $.ajax({
        url : basePath + "data/del?deleteAttr=" + deleteAttr,
        dataType: "json",
        type:"POST",
        success: function(returnData) {
            alert("操作成功");
            clm = buildAttr(getAttr());
            initTable();
        },
        error: doError
    });
}

// 缺失值处理
function missValueDeal() {
    var isChecked = $("input[name='missValue']:checked").val();
    // 全部删除
    if (isChecked == "miss-del") {
        var newDataSet = new Array();
        for (var i = 0; i < dataSet.length; i++) {
            newDataSet[i] = new Array();
            for (var j = 0; j < dataSet[0].length; j++) {
                newDataSet[i][j] = 1;
            }
        }
        var missLine = 0;
        for (var line = 0; line < dataSet.length; line++) {
            for (var row = 0; row < dataSet[0].length; row++) {
                if (dataSet[line][row] == "") {
                    missLine += 1;
                    break;//如果发现该行有空值，下次复制数据到newDataSet的时候重写该行
                }
                newDataSet[line - missLine][row] = dataSet[line][row];
            }
        }
        for (var i = 0; i < missLine; i++) {
            newDataSet[dataSet.length - missLine + i].splice(0);
        }
        dataClean(rebuildNewDataSet(newDataSet, missLine));
    }
    if (isChecked == "miss-avrage") {
        for (var line = 0; line < dataSet.length; line++) {
            var sum = 0;
            var mod = 0;
            for (var row = 0; row < dataSet[0].length; row++) {
                if (dataSet[line][row] == "") {
                    if (line == 0) {
                        var compareLine = line + 1;
                    } else {
                        var compareLine = line - 1;
                    }
                    if (isNaN((dataSet[compareLine][row]))) {
                        dataSet[line][row] = dataSet[compareLine][row];
                    } else {
                        for (var i = 0; i < dataSet.length; i++) {//是数字就计算出平均值，位数有点多，不知道保留几位
                            if (dataSet[i][row] != "") {
                                sum += Number(dataSet[i][row]);
                            } else {
                                mod += 1;
                            }
                        }
                        dataSet[line][row] = sum / (dataSet.length - mod)
                    }
                }
            }
        }
        dataClean(dataSet);
    }
    if (isChecked == "miss-max") {
        for (var line = 0; line < dataSet.length; line++) {
            var max = 0;
            for (var row = 0; row < dataSet[0].length; row++) {
                if (dataSet[line][row] == "") {
                    if (line == 0) {
                        var compareLine = line + 1;
                    } else {
                        var compareLine = line - 1;
                    }
                    if (isNaN((dataSet[compareLine][row]))) {
                        dataSet[line][row] = dataSet[compareLine][row];
                    } else {
                        for (var i = 0; i < dataSet.length; i++) {//是数字就计算出最大值
                            if (dataSet[i][row] != "" && Number(dataSet[i][row]) > max) {
                                max = dataSet[i][row];
                            }
                        }
                        dataSet[line][row] = max;
                    }
                }
            }
        }
        dataClean(dataSet);
    }
    if (isChecked == "miss-min") {
        for (var line = 0; line < dataSet.length; line++) {
            var min = 99999999;
            for (var row = 0; row < dataSet[0].length; row++) {
                if (dataSet[line][row] == "") {
                    if (line == 0) {
                        var compareLine = line + 1;
                    } else {
                        var compareLine = line - 1;
                    }
                    if (isNaN((dataSet[compareLine][row]))) {
                        dataSet[line][row] = dataSet[compareLine][row];
                    } else {
                        for (var i = 0; i < dataSet.length; i++) {//是数字就计算出最小值
                            if (dataSet[i][row] != "") {
                                if (Number(dataSet[i][row]) < min) {
                                    min = dataSet[i][row];
                                }
                            }
                        }
                        dataSet[line][row] = min;
                    }
                }
            }
        }
        dataClean(dataSet);
    }
    if (isChecked == "miss-zero") {
        for (var line = 0; line < dataSet.length; line++) {
            for (var row = 0; row < dataSet[0].length; row++) {
                if (dataSet[line][row] == "") {
                    if (line == 0) {
                        var compareLine = line + 1;
                    } else {
                        var compareLine = line - 1;
                    }
                    if (isNaN((dataSet[compareLine][row]))) {
                        dataSet[line][row] = dataSet[compareLine][row];//如果是字符就找到下一行复制过来（其实有BUG，下一行可能没有了）
                    } else {
                        dataSet[line][row] = "0";
                    }
                }
            }
        }
        dataClean(dataSet);
    }
    alert("操作成功！  共清洗 ：" + missLine + "行");
    document.getElementById("dis2").className = "btn btn-primary disabled";
}
// 离群值处理
function departValueDeal() {
    var isChecked = $("input[name='departValue']:checked").val();
    // 全部删除
    if (isChecked == "depart-del") {
        var newDataSet = new Array();
        for (var i = 0; i < dataSet.length; i++) {
            newDataSet[i] = new Array();
            for (var j = 0; j < dataSet[0].length; j++) {
                newDataSet[i][j] = 1;
            }
        }
        var departLine = 0;
        var sum;
        var avrge;
        for (var line = 0; line < dataSet.length; line++) {
            sum = 0;
            avrge=0;
            for (var row = 0; row < dataSet[0].length; row++) {
                sum = 0;
                avrge=0;
                if (!isNaN((dataSet[line][row]))) {
                    for (var depart = 0; depart < dataSet.length; depart++) {
                        if (isNaN((dataSet[depart][row]))) {
                            continue;
                        }
                        sum += Number(dataSet[depart][row]);
                    }
                }
                avrge = (sum - Number(dataSet[line][row]))/(dataSet.length - 1);
                if (isNaN((dataSet[line][row]))) {
                    newDataSet[line - departLine][row] = dataSet[line][row];
                    continue;
                } else if (Number(dataSet[line][row]) / avrge > 10
                        || Number(dataSet[line][row]) / avrge < 0.1){
                    departLine += 1;
                    dataSet[line][row] = avrge;
                    break;
                }
                newDataSet[line - departLine][row] = dataSet[line][row];
            }
        }
        for (var i = 0; i < departLine; i++) {
            newDataSet[dataSet.length - departLine + i].splice(0);
        }
        alert("操作成功！  共清洗 ：" + departLine + "行");
        dataClean(rebuildNewDataSet(newDataSet, departLine));
    }
    if (isChecked == "depart-edge") {
        var departLine = 0;
        var sum;
        var avrge;
        for (var line = 0; line < dataSet.length; line++) {
            sum = 0;
            avrge=0;
            for (var row = 0; row < dataSet[0].length; row++) {
                sum = 0;
                avrge=0;
                if (!isNaN((dataSet[line][row]))) {
                    for (var depart = 0; depart < dataSet.length; depart++) {
                        if (isNaN((dataSet[depart][row]))) {
                            continue;
                        }
                        sum += Number(dataSet[depart][row]);
                    }
                }
                avrge = (sum - Number(dataSet[line][row]))/(dataSet.length - 1);
                if (isNaN((dataSet[line][row]))) {
                    continue;
                } else if (Number(dataSet[line][row]) / avrge > 10
                        || Number(dataSet[line][row]) / avrge < 0.1){
                    departLine += 1;
                    dataSet[line][row] = avrge;
                    break;
                }
            }
        }
        alert("操作成功！  共清洗 ：" + departLine + "行");
        dataClean(dataSet);
    }
    document.getElementById("dis3").className = "btn btn-primary disabled";

}

function dataClean(dataSet) {
    var attr = getAttr();
    $.ajax({
        url: basePath + "data/overrideDataSet?dataSet=" + dataSet + "&attr=" + attr,
        dataType: "json",
        type: "POST",
        success: function (returnData) {
            initTable();
        },
        error: doError
    });
}

function rebuildDataSet(newDataSet) {

    var reDAtaSet = [];
    for (var i = 0; i < newDataSet.length; i++) {
        reDAtaSet = newDataSet[i];
    }
    return reDAtaSet;
}

function rebuildNewDataSet(newDataSet, missLine) {
    var reNewDataSet = [];
    for (var i = 0; i < newDataSet.length - missLine; i++) {
        reNewDataSet[i] = newDataSet[i];
    }
    return reNewDataSet;
}

$(function () {
    initTable();
    buildType(getAttr());
})

</script>
</body>
</html>