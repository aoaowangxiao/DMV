<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-6-4
  Time: 下午21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>平行坐标图</title>
    <%@include file="../../includes/d3common.jsp" %>
    <style>

        text {
            font-family: "Helvetica Neue", Helvetica, sans-serif;
        }

        .name {
            font-weight: bold;
        }

        .about {
            fill: #777;
            font-size: smaller;
        }

        .link {
            fill: none;
            stroke: #000;
            shape-rendering: crispEdges;
        }

    </style>
</head>
<body>
<%@ include file="../../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        树形图
        <small>分类结果</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/scatter/showScatter">可视化</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">分类</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 树形图</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="padding: 20px;height: 520px;"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
<script>

    var margin = {top: 0, right: 220, bottom: 0, left: 0},
            width = 1200 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

    var tree = d3.layout.tree()
            .separation(function(a, b) { return a.parent === b.parent ? 1 : 1; })
            .children(function(d) { return d.parents; })
            .size([height, width]);

    var svg = d3.select("#main").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3.json("tree.json", function(json) {
        var nodes = tree.nodes(json);

        var link = svg.selectAll(".link")
                .data(tree.links(nodes))
                .enter().append("path")
                .attr("class", "link")
                .attr("d", elbow);

        var node = svg.selectAll(".node")
                .data(nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })

        node.append("text")
                .attr("class", "name")
                .attr("x", 108)
                .attr("y", -6)
                .text(function(d) { return d.name; });

//        node.append("text")
//                .attr("x", 8)
//                .attr("y", 8)
//                .attr("dy", ".71em")
//                .attr("class", "about lifespan")
//                .text(function(d) { return d.born + "–" + d.died; });

        node.append("text")
                .attr("x", 8)
                .attr("y", -28)
                .attr("dy", "1.86em")
                .attr("class", "about condition")
                .text(function(d) { return d.condition; });
    });

    function elbow(d, i) {
        return "M" + d.source.y + "," + d.source.x
                + "H" + d.target.y + "V" + d.target.x
                + (d.target.children ? "" : "h" + margin.right);
    }

</script>
</body>
</html>