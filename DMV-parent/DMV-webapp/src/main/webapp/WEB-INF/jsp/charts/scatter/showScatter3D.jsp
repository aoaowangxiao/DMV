<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>三维散点图</title>
    <%@include file="../../includes/common.jsp" %>
    <script type="text/javascript" src="<c:url value="/js/common/canvasXpress/js/canvasXpress.min.js"/>"></script>
</head>
<body onload="showDemo();">
<%@ include file="../../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        三维散点图
        <small>显示三维散点图</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/scatter/showScatter">可视化</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">三维散点图</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 三维散点图</h3>
        </div>
        <div class="panel-body" style="margin-left: 25%;">

               <canvas id='canvas1' width='600' height='600'></canvas>

        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
<script id='demoScript'>

    function getValues() {
        var objArr;
        $.ajax({
            url: basePath + "charts/scatter/getPoint",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnArr) {
                objArr = returnArr;
            },
            error: doError
        });
        return objArr;
    }

    function getAttr() {
        var arr;
        $.ajax({
            url: basePath + "data/getAttr",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                arr = returnData;
            },
            error: doError
        });
        return arr;
    }

    function buildVars(returnData) {
        var vars = [];
        for (var i = 0; i < returnData.length; i++) {
            vars[i] = 'Variable' + (i + 1);
        }
        return vars;
    }

    function buildData(returnData) {
        var data = [];
        for (var i = 0; i < returnData.length; i++) {
            data[i] = returnData[i];
        }
        return data;
    }



    var showDemo = function () {
        var cx1 = new CanvasXpress('canvas1',
                {
                    'y': {
                        'vars': buildVars(getValues()),
                        'smps': getAttr(),
                        'data': buildData(getValues())
                    }
                },
                {'graphType': 'Scatter3D',
                    'xAxis': ['X'],
                    'yAxis': ['Y'],
                    'zAxis': ['Z']}
        );
    }

    var showCode = function (e, id) {
        var cx = CanvasXpress.getObject(id)
        cx.stopEvent(e);
        cx.cancelEvent(e);
        cx.updateCodeDiv(10000);
        return false;
    }


</script>
