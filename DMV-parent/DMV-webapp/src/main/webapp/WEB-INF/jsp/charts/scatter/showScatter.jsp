<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-4-15
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>散点图</title>
    <%@include file="../../includes/common.jsp" %>
</head>
<body>
<%@ include file="../../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        散点图
        <small>显示散点图</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/scatter/showScatter">可视化</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">散点图</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 散点图</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="height:600px;margin-top: 20px;"></div>
            <div id="tryDiv"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
<script type="text/javascript">
    function getValues() {
        var objArr;
        $.ajax({
            url: basePath + "charts/scatter/getPoint",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnArr) {
                objArr = returnArr;
            },
            error: doError
        });
        return objArr;
    }
    require.config({
        paths: {
            'echarts': './js/common/echarts/echarts-original',
            'echarts/chart/scatter': './js/common/echarts/echarts-original'
        }
    });
    require(
            [
                'echarts',
                'echarts/chart/scatter' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));
                var dataArr = getValues();

                option = {
                    tooltip : {
                        trigger: 'axis',
                        showDelay : 0,
                        axisPointer:{
                            type : 'cross',
                            lineStyle: {
                                type : 'dashed',
                                width : 1
                            }
                        }

                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataZoom : {show: true},

                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    xAxis : [
                        {
                            type : 'value',
                            power: 1,
                            precision: 2,
                            scale:true

                        }
                    ],
                    yAxis : [
                        {
                            type : 'value',
                            power: 1,
                            precision: 2,
                            scale:true
                        }
                    ],
                    series : [
                        {
                            type:'scatter',
                            data:dataArr,
                            markPoint : {
                                data : [
                                    {type : 'max', name: '最大值'},
                                    {type : 'min', name: '最小值'}
                                ]
                            },
                            markLine : {
                                data : [
                                    {type : 'average', name: '平均值'}
                                ]
                            }
                        }
                    ]
                };

                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );
</script>