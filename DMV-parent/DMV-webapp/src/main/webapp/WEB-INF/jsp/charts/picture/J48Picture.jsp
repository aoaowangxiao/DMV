<%--
  Created by IntelliJ IDEA.
  User: Peter
  Date: 14-6-26
  Time: 下午4:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>分类</title>
    <%@include file="../../includes/common.jsp" %>
</head>
<body>
<%@ include file="../../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        分类(J48)
        <small>分类可视化结果</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/picture/J48Picture">数据挖掘</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">分类(J48)</li>
    </ol>
    <div id="app"></div>
<button onclick="test()">测试</button>
</div>
</body>
</html>
<script type="text/javascript">
function test(){
    var test;
    $.ajax({
        url: basePath + "charts/picture/doJ48",
        dataType: "text",
        type: "POST",
        success: function (returnArr) {
            test = returnArr;
            $("#app").html(returnArr);
        },
        error: doError
    });
    return test;
}
</script>