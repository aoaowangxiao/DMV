<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-4-15
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>聚类</title>
    <%@include file="../../includes/common.jsp" %>
</head>
<body>
<%@ include file="../../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        聚类
        <small>聚类可视化结果</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/scatter/kmeansScatter">数据挖掘</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">聚类</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 聚类结果（散点图）</h3>
            <div class="btn-group" style="float: right;margin-top: -25px;">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                    散点图 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="charts/scatter/kmeansScatter">散点图</a></li>
                    <li><a href="charts/scatter/kmeansRadar">雷达图</a></li>
                    <li><a href="charts/scatter/kmeansParallel">平行坐标</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="height:600px;margin-top: 20px;"></div>
            <div id="tryDiv"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:left;width: 39%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 饼图分析</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="pie" style="height:500px;margin-top: 20px;"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:right;width: 60%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 结果数据</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="datatable" style="height:520px;"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
<script type="text/javascript">
    var pieBelong = [0,0,0];
    require.config({
        paths: {
            'echarts': './js/common/echarts/echarts',
            'echarts/chart/scatter': './js/common/echarts/echarts',
            'echarts/chart/pie': './js/common/echarts/echarts'
        }
    });
    require(
            [
                'echarts',
                'echarts/chart/scatter',
                'echarts/chart/pie'
            ],
            function (ec) {
                myChart = ec.init(document.getElementById('main'));
                var option = getOption(getValues());
                myChart.setOption(option);
                myPie = ec.init(document.getElementById('pie'));
                myPie.setOption({
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient : 'vertical',
                        x : 'left',
                        data:['聚类0','聚类1','聚类2']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    series : [
                        {
                            type:'pie',
                            radius : '55%',
                            center: ['50%', 250],
                            data:[
                                {value:pieBelong[0], name:'聚类0'},
                                {value:pieBelong[1], name:'聚类1'},
                                {value:pieBelong[2], name:'聚类2'}
                            ]
                        }
                    ]
                });
            }
    );

    function getValues() {
        var returnObj;
        $.ajax({
            url: basePath + "charts/scatter/doKMeans",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnArr) {
                returnObj = rebuild(returnArr);
            },
            error: doError
        });
        return returnObj;
    }

    function rebuild(sourceObj) {
        var numOfClusterer = sourceObj.NumOfClusterer;
        var sourceData =  sourceObj.Data;
        var belong = sourceObj.EachBelong;
        var data = [];
        var type = [];
        var typeIndex;
        var returnObj;
        for (var i = 0; i < numOfClusterer; ++i) {
            type[i] = 0;
            data[i] = [];
        }
        for (var item in sourceData) {
            if(belong[item]==0){
               pieBelong[0]+=1;
            }
            if(belong[item]==1){
               pieBelong[1]+=1;
            }
            if(belong[item]==2){
               pieBelong[2]+=1;
            }
            typeIndex = belong[item];
            data[typeIndex][type[typeIndex]] = sourceData[item];
            ++type[typeIndex];
        }
        returnObj = {};
        returnObj.NumOfClusterer = numOfClusterer;
        returnObj.CapitalPoints = sourceObj.CapitalPoints;
        returnObj.Data = data;
        return returnObj;
    }

    function getOption(dataObj) {
        var option = {
            tooltip: {
                trigger: 'item',
                formatter: pointType
            },
            legend: setLegend(dataObj.NumOfClusterer),
            toolbox: {
                show: true,
                feature: {
                    mark: {show: true},
                    dataZoom: {show: true},
                    dataView: {show: true, readOnly: false},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            xAxis: [
                {
                    type: 'value',
                    power: 1,
                    precision: 2,
                    scale: true,
                    axisLabel: {
                        formatter: '{value} m'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    power: 1,
                    precision: 2,
                    scale: true,
                    axisLabel: {
                        formatter: '{value} '
                    },
                    splitArea: {show: true}
                }
            ],
            series: setSeries(dataObj.NumOfClusterer, dataObj.CapitalPoints, dataObj.Data)
        };
        return option;
    }

    function setSeries(numOfClusterer, capitalPoints, dataSet) {
        var returnArr = [];
        for (var i = 0; i < numOfClusterer; ++i) {
            returnArr[i] = {
                name: '聚类' + i,
                type: 'scatter',
                data: dataSet[i],
                markPoint: {
                    data: [
                        {name: ['聚类' + i + '中心点', capitalPoints[i][0], capitalPoints[i][1]], xAxis: capitalPoints[i][0], yAxis: capitalPoints[i][1]}
                    ]
                }
            }
        }
        return returnArr;
    }

    function setLegend(numOfClusterer) {
        var returnObj = {data: []};
        for (var i = 0; i < numOfClusterer; ++i) {
            returnObj.data[i] = '聚类' + i;
        }
        return returnObj;
    }

    function pointType(value) {
        if (value[2].length > 1) {
            return value[0] + ' :<br/>'
                    + value[2][0]
                    + value[2][1] ;
        }
        return value[1][0] + ' :<br/>'
                + value[1][1]
                + value[1][2] ;
    }

</script>
<script type="text/javascript">

    var dataSet = [];
    var columsSet = [];

    function initTable() {
        getAttr();
        getDataSet();
        $('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
        var table = $('#example').DataTable({
                    "data": dataSet,
                    "columns": columsSet,
                    "bLengthChange": false
                }
        );
        $('.toggle-vis').on('click', function (e) {
            var thisObj = $(this);
            e.preventDefault();
            // Get the column API object
            var column = table.column($(this).attr('data-column'));
            // Toggle the visibility
            column.visible(!column.visible());
            thisObj.find("i").toggleClass("icon-check-empty")
        });
    }

    function getAttr() {
        var arr = [];
        $.ajax({
            url: basePath + "data/getAttr",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                columsSet = buildAttr(returnData);
            },
            error: doError
        });
        return arr;
    }

    function buildAttr(returnData) {
        var arr = [];
        for (var i = 0; i < returnData.length; i++) {
            arr[i] = { "sTitle": returnData[i] };
        }
        return arr;
    }

    function getDataSet() {
        $.ajax({
            url: basePath + "data/getdata",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                dataSet = buildDataSet(returnData);
            },
            error: doError
        });
    }

    function buildDataSet(returnData) {
        var dataSet = [];
        for (var i = 0; i < returnData.length; i++) {
            dataSet[i] = returnData[i];
        }
        return dataSet;
    }

    $(function () {
        initTable();
    })

</script>