<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-6-7
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>聚类</title>
    <%@include file="../../includes/d3common.jsp" %>
    <style type="text/css">

        svg {
            font-size: 14px;
        }

        .foreground path {
            fill: none;
            stroke-opacity: .5;
            stroke-width: 1.5px;
        }

        .foreground path.fade {
            stroke: #000;
            stroke-opacity: .05;
        }

        .legend {
            font-size: 18px;
            font-style: oblique;
        }

        .legend line {
            stroke-width: 2px;
        }

        .setosa {
            stroke: #800;
        }

        .versicolor {
            stroke: #080;
        }

        .virginica {
            stroke: #008;
        }

        .brush .extent {
            fill-opacity: .3;
            stroke: #fff;
            shape-rendering: crispEdges;
        }

        .axis line, .axis path {
            fill: none;
            stroke: #000;
            shape-rendering: crispEdges;
        }

        .axis text {
            text-shadow: 0 1px 0 #fff;
            cursor: move;
        }

    </style>
</head>
<body>
<%@ include file="../../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        聚类
        <small>聚类可视化结果</small>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/scatter/kmeansScatter">数据挖掘</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">聚类</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 聚类结果（平行坐标）</h3>
            <div class="btn-group" style="float: right;margin-top: -25px;">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                    平行坐标 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="charts/scatter/kmeansScatter">散点图</a></li>
                    <li><a href="charts/scatter/kmeansRadar">雷达图</a></li>
                    <li><a href="charts/scatter/kmeansParallel">平行坐标</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="height:650px;"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 结果数据</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="datatable" style="height:520px;"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
<script type="text/javascript">

var species = ["setosa", "versicolor", "virginica"],
        traits = ["sepal length", "petal length", "sepal width", "petal width"];

var m = [80, 160, 200, 160],
        w = 1280 - m[1] - m[3],
        h = 800 - m[0] - m[2];

var x = d3.scale.ordinal().domain(traits).rangePoints([0, w]),
        y = {};

var line = d3.svg.line(),
        axis = d3.svg.axis().orient("left"),
        foreground;

var svg = d3.select("#main").append("svg:svg")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2])
        .append("svg:g")
        .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

d3.csv("iris.csv", function(flowers) {

    // Create a scale and brush for each trait.
    traits.forEach(function(d) {
        // Coerce values to numbers.
        flowers.forEach(function(p) { p[d] = +p[d]; });

        y[d] = d3.scale.linear()
                .domain(d3.extent(flowers, function(p) { return p[d]; }))
                .range([h, 0]);

        y[d].brush = d3.svg.brush()
                .y(y[d])
                .on("brush", brush);
    });

    // Add a legend.
//    var legend = svg.selectAll("g.legend")
//            .data(species)
//            .enter().append("svg:g")
//            .attr("class", "legend")
//            .attr("transform", function(d, i) { return "translate(0," + (i * 20 + 584) + ")"; });
//
//    legend.append("svg:line")
//            .attr("class", String)
//            .attr("x2", 8);
//
//    legend.append("svg:text")
//            .attr("x", 12)
//            .attr("dy", ".31em")
//            .text(function(d) { return "Iris " + d; });

    // Add foreground lines.
    foreground = svg.append("svg:g")
            .attr("class", "foreground")
            .selectAll("path")
            .data(flowers)
            .enter().append("svg:path")
            .attr("d", path)
            .attr("class", function(d) { return d.species; });

    // Add a group element for each trait.
    var g = svg.selectAll(".trait")
            .data(traits)
            .enter().append("svg:g")
            .attr("class", "trait")
            .attr("transform", function(d) { return "translate(" + x(d) + ")"; })
            .call(d3.behavior.drag()
                    .origin(function(d) { return {x: x(d)}; })
                    .on("dragstart", dragstart)
                    .on("drag", drag)
                    .on("dragend", dragend));

    // Add an axis and title.
    g.append("svg:g")
            .attr("class", "axis")
            .each(function(d) { d3.select(this).call(axis.scale(y[d])); })
            .append("svg:text")
            .attr("text-anchor", "middle")
            .attr("y", -9)
            .text(String);

    // Add a brush for each axis.
    g.append("svg:g")
            .attr("class", "brush")
            .each(function(d) { d3.select(this).call(y[d].brush); })
            .selectAll("rect")
            .attr("x", -8)
            .attr("width", 16);

    function dragstart(d) {
        i = traits.indexOf(d);
    }

    function drag(d) {
        x.range()[i] = d3.event.x;
        traits.sort(function(a, b) { return x(a) - x(b); });
        g.attr("transform", function(d) { return "translate(" + x(d) + ")"; });
        foreground.attr("d", path);
    }

    function dragend(d) {
        x.domain(traits).rangePoints([0, w]);
        var t = d3.transition().duration(500);
        t.selectAll(".trait").attr("transform", function(d) { return "translate(" + x(d) + ")"; });
        t.selectAll(".foreground path").attr("d", path);
    }
});

// Returns the path for a given data point.
function path(d) {
    return line(traits.map(function(p) { return [x(p), y[p](d[p])]; }));
}

// Handles a brush event, toggling the display of foreground lines.
function brush() {
    var actives = traits.filter(function(p) { return !y[p].brush.empty(); }),
            extents = actives.map(function(p) { return y[p].brush.extent(); });
    foreground.classed("fade", function(d) {
        return !actives.every(function(p, i) {
            return extents[i][0] <= d[p] && d[p] <= extents[i][1];
        });
    });
}



var dataSet = [];
    var columsSet = [];

    function initTable() {
        getAttr();
        getDataSet();
        $('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
        var table = $('#example').DataTable({
                    "data": dataSet,
                    "columns": columsSet,
                    "bLengthChange": false
                }
        );
        $('.toggle-vis').on('click', function (e) {
            var thisObj = $(this);
            e.preventDefault();
            // Get the column API object
            var column = table.column($(this).attr('data-column'));
            // Toggle the visibility
            column.visible(!column.visible());
            thisObj.find("i").toggleClass("icon-check-empty")
        });
    }

    function getAttr() {
        var arr = [];
        $.ajax({
            url: basePath + "data/getAttr",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                columsSet = buildAttr(returnData);
            },
            error: doError
        });
        return arr;
    }

    function buildAttr(returnData) {
        var arr = [];
        for (var i = 0; i < returnData.length; i++) {
            arr[i] = { "sTitle": returnData[i] };
        }
        return arr;
    }

    function getDataSet() {
        $.ajax({
            url: basePath + "data/getdata",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnData) {
                dataSet = buildDataSet(returnData);
            },
            error: doError
        });
    }

    function buildDataSet(returnData) {
        var dataSet = [];
        for (var i = 0; i < returnData.length; i++) {
            dataSet[i] = returnData[i];
        }
        return dataSet;
    }

    $(function () {
        initTable();
    })
</script>