<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-4-15
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>聚类</title>
    <%@include file="../includes/common.jsp" %>
</head>
<body>
<%@ include file="../includes/guideMenu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        聚类
        <small>聚类可视化结果</small>
        <a href="guide/visual" style="float: right" class="button button-flat"><i class="icon-circle-arrow-left"></i> 返回</a>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">向导模式</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="guide/visual">可视化</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">聚类</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 聚类结果（雷达图）</h3>

            <div class="btn-group" style="float: right;margin-top: -25px;">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                    雷达图 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="guide/kmeans">散点图</a></li>
                    <li><a href="guide/kmeansRadar">雷达图</a></li>
                    <li><a href="guide/kmeansParallel">平行坐标</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="height:600px;margin-top: 20px;"></div>
            <div id="tryDiv"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:left;width: 39%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 饼图分析</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="pie" style="height:500px;margin-top: 20px;"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:right;width: 60%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 结果数据</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="datatable" style="height:520px;"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
<script type="text/javascript">
require.config({
    paths: {
        'echarts': './js/common/echarts/echarts',
        'echarts/chart/scatter': './js/common/echarts/echarts',
        'echarts/chart/pie': './js/common/echarts/echarts'
    }
});
require(
        [
            'echarts',
            'echarts/chart/radar',
            'echarts/chart/pie'
        ],
        function (ec) {
            myChart = ec.init(document.getElementById('main'));
            myChart.setOption({
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    y: 'top',
                    data: ['聚类1', '聚类2', '聚类3']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {show: true, readOnly: false},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                polar: [
                    {
                        indicator: [
                            { text: 'sepallength', max: 8},
                            { text: 'sepalwidth', max: 5},
                            { text: 'petallength', max: 7},
                            { text: 'petalwidth', max: 3}
                        ]
                    }
                ],
                calculable: true,
                series: [
                    {
                        type: 'radar',
                        data: [
                            {
                                value: [5.1, 3.5, 1.4, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [4.9, 3, 1.4, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [4.7, 3.2, 1.3, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [4.6, 3.1, 1.5, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [5, 3.6, 1.4, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [5.4, 3.9, 1.7, 0.4],
                                name: '聚类1'
                            },
                            {
                                value: [4.6, 3.4, 1.4, 0.3],
                                name: '聚类1'
                            },
                            {
                                value: [5, 3.4, 1.5, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [4.4, 2.9, 1.4, 0.2],
                                name: '聚类1'
                            },
                            {
                                value: [4.9, 3.1, 1.5, 0.1],
                                name: '聚类1'
                            },
                            {
                                value: [7, 3.2, 4.7, 1.4],
                                name: '聚类2'
                            },
                            {
                                value: [6.4, 3.2, 4.5, 1.5],
                                name: '聚类2'
                            },
                            {
                                value: [6.9, 3.1, 4.9, 1.5],
                                name: '聚类2'
                            },
                            {
                                value: [5.5, 2.3, 4, 1.3],
                                name: '聚类2'
                            },
                            {
                                value: [6.5, 2.8, 4.6, 1.5],
                                name: '聚类2'
                            },
                            {
                                value: [5.7, 2.8, 4.5, 1.3],
                                name: '聚类2'
                            },
                            {
                                value: [6.3, 3.3, 4.7, 1.6],
                                name: '聚类2'
                            },
                            {
                                value: [4.9, 2.4, 3.3, 1],
                                name: '聚类2'
                            },
                            {
                                value: [6.6, 2.9, 4.6, 1.3],
                                name: '聚类2'
                            },
                            {
                                value: [5.2, 2.7, 3.9, 1.4],
                                name: '聚类2'
                            },
                            {
                                value: [6.3, 3.3, 6, 2.5],
                                name: '聚类3'
                            },
                            {
                                value: [5.8, 2.7, 5.1, 1.9],
                                name: '聚类3'
                            },
                            {
                                value: [7.1, 3, 5.9, 2.1],
                                name: '聚类3'
                            },
                            {
                                value: [6.3, 2.9, 5.6, 1.8],
                                name: '聚类3'
                            },
                            {
                                value: [6.5, 3, 5.8, 2.2],
                                name: '聚类3'
                            },
                            {
                                value: [7.6, 3, 6.6, 2.1],
                                name: '聚类3'
                            },
                            {
                                value: [4.9, 2.5, 4.5, 1.7],
                                name: '聚类3'
                            },
                            {
                                value: [7.3, 2.9, 6.3, 1.8],
                                name: '聚类3'
                            },
                            {
                                value: [6.7, 2.5, 5.8, 1.8],
                                name: '聚类3'
                            },
                            {
                                value: [7.2, 3.6, 6.1, 2.5],
                                name: '聚类3'
                            }
                        ]
                    }
                ]
            });
            myPie = ec.init(document.getElementById('pie'));
            myPie.setOption({
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['聚类1', '聚类2', '聚类3']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {show: true, readOnly: false},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                calculable: true,
                series: [
                    {
//                            name:'访问来源',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', 250],
                        data: [
                            {value: 36, name: '聚类1'},
                            {value: 47, name: '聚类2'},
                            {value: 33, name: '聚类3'}
                        ]
                    }
                ]
            });
        }
);


function rebuild(sourceObj) {
    var numOfClusterer = sourceObj.NumOfClusterer;
    var sourceData = sourceObj.Data;
    var belong = sourceObj.EachBelong;
    var data = [];
    var type = [];
    var typeIndex;
    var returnObj;
    for (var i = 0; i < numOfClusterer; ++i) {
        type[i] = 0;
        data[i] = [];
    }
    for (var item in sourceData) {
        typeIndex = belong[item];
        data[typeIndex][type[typeIndex]] = sourceData[item];
        ++type[typeIndex];
    }
    returnObj = {};
    returnObj.NumOfClusterer = numOfClusterer;
    returnObj.CapitalPoints = sourceObj.CapitalPoints;
    returnObj.Data = data;
    return returnObj;
}

function getOption(dataObj) {
    var
            option = {
                title: {
                    text: '预算 vs 开销（Budget vs spending）',
                    subtext: '纯属虚构'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    orient: 'vertical',
                    x: 'right',
                    y: 'bottom',
                    data: ['预算分配（Allocated Budget）', '实际开销（Actual Spending）']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {show: true, readOnly: false},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                polar: [
                    {
                        indicator: [
                            { text: '销售（sales）', max: 6000},
                            { text: '管理（Administration）', max: 16000},
                            { text: '信息技术（Information Techology）', max: 30000},
                            { text: '客服（Customer Support）', max: 38000},
                            { text: '研发（Development）', max: 52000},
                            { text: '市场（Marketing）', max: 25000}
                        ]
                    }
                ],
                calculable: true,
                series: [
                    {
                        name: '预算 vs 开销（Budget vs spending）',
                        type: 'radar',
                        data: [
                            {
                                value: [4300, 10000, 28000, 35000, 50000, 19000],
                                name: '预算分配（Allocated Budget）'
                            },
                            {
                                value: [5000, 14000, 28000, 31000, 42000, 21000],
                                name: '实际开销（Actual Spending）'
                            }
                        ]
                    }
                ]
            };
    return option;
}

function setSeries(numOfClusterer, capitalPoints, dataSet) {
    var returnArr = [];
    for (var i = 0; i < numOfClusterer; ++i) {
        returnArr[i] = {
            name: '聚类' + i,
            type: 'scatter',
            data: dataSet[i],
            markPoint: {
                data: [
                    {name: ['聚类' + i + '中心点', capitalPoints[i][0], capitalPoints[i][1]], xAxis: capitalPoints[i][0], yAxis: capitalPoints[i][1]}
                ]
            }
        }
    }
    return returnArr;
}

function setLegend(numOfClusterer) {
    var returnObj = {data: []};
    for (var i = 0; i < numOfClusterer; ++i) {
        returnObj.data[i] = '聚类' + i;
    }
    return returnObj;
}

function pointType(value) {
    if (value[2].length > 1) {
        return value[0] + ' :<br/>'
                + value[2][0] + 'm '
                + value[2][1] + 'm ';
    }
    return value[1][0] + ' :<br/>'
            + value[1][1] + 'm '
            + value[1][2] + 'm ';
}

var dataSet = [];
var columsSet = [];

function initTable() {
    getAttr();
    getDataSet();
    $('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
    var table = $('#example').DataTable({
                "data": dataSet,
                "columns": columsSet,
                "bLengthChange": false
            }
    );
    $('.toggle-vis').on('click', function (e) {
        var thisObj = $(this);
        e.preventDefault();
        // Get the column API object
        var column = table.column($(this).attr('data-column'));
        // Toggle the visibility
        column.visible(!column.visible());
        thisObj.find("i").toggleClass("icon-check-empty")
    });
}

function getAttr() {
    var arr = [];
    $.ajax({
        url: basePath + "data/getAttr",
        dataType: "json",
        type: "POST",
        async: false,
        success: function (returnData) {
            columsSet = buildAttr(returnData);
        },
        error: doError
    });
    return arr;
}

function buildAttr(returnData) {
    var arr = [];
    for (var i = 0; i < returnData.length; i++) {
        arr[i] = { "sTitle": returnData[i] };
    }
    return arr;
}

function getDataSet() {
    $.ajax({
        url: basePath + "data/getdata",
        dataType: "json",
        type: "POST",
        async: false,
        success: function (returnData) {
            dataSet = buildDataSet(returnData);
        },
        error: doError
    });
}

function buildDataSet(returnData) {
    var dataSet = [];
    for (var i = 0; i < returnData.length; i++) {
        dataSet[i] = returnData[i];
    }
    return dataSet;
}

$(function () {
    initTable();
})
</script>