<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-6-4
  Time: 下午21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>平行坐标图</title>
    <%@include file="../includes/d3common.jsp" %>
</head>
<body>
<%@ include file="../includes/guideMenu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        平行坐标<small>关联规则结果</small>
        <a href="guide/visual" style="float: right" class="button button-flat"><i class="icon-circle-arrow-left"></i> 返回</a>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">向导模式</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="guide/visual">可视化</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">关联规则</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 平行坐标</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="padding: 20px;height: 520px;"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-table"></i> 关联规则结果</h3>
        </div>
        <div class="panel-body">
            <div id="datatable" style="padding: 20px;height: 520px;"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
<script type="text/javascript">


    //Width and height
    var w = document.getElementsByClassName("panel-body").offsetWidth;;
    var h = 500;

    var dataset = [
        [10,0,13,0,"#063D6D","sunny"],
        [10,75,13,75,"#063D6D","overcast"],
        [10,150,13,150,"#063D6D","rainy"],
        [110,0,113,0,"#063D6D","hot"],
        [110,75,113,75,"#063D6D","mild"],
        [110,150,113,150,"#063D6D","cool"],
        [210,0,213,0,"#063D6D","high"],
        [210,150,213,150,"#063D6D","normal"],
        [310,0,313,0,"#063D6D","TRUE"],
        [310,150,313,150,"#063D6D","FALSE"],
        [410,0,413,0,"#063D6D","yes"],
        [410,150,413,150,"#063D6D","no"],
        [10,0,10,150,"#063D6D"],//Y轴1
        [0,160,0,160,"#000","outlook"],//Y轴注释1
        [110,0,110,150,"#063D6D"],//Y轴2
        [90,160,90,160,"#000","temperature"],//Y轴注释2
        [210,0,210,150,"#063D6D"],//Y轴3
        [200,160,200,160,"#000","humidity"],//Y轴注释3
        [310,0,310,150,"#063D6D"],//Y轴4
        [300,160,300,160,"#000","windy"],//Y轴注释4
        [410,0,410,150,"#063D6D"],//Y轴5
        [400,160,400,160,"#000","play"],//Y轴注释5
        [10,0,210,0,"rgba(66, 139, 202, 0.5)"],
        [10,0,410,150,"rgba(202, 66, 77, 0.5)"],
        [10,75,410,0,"rgba(202, 66, 186, 0.5)"],
        [10,150,410,0,"rgba(66, 175, 202, 0.5)"],
        [10,150,310,150,"rgba(66, 202, 142, 0.5)"],
        [110,150,410,0,"rgba(110, 202, 66, 0.5)"],
        [110,150,210,150,"rgba(202, 66, 115, 0.5)"],
        [210,0,410,150,"rgba(66, 139, 202, 0.5)"],
        [210,0,410,150,"rgba(66, 139, 202, 0.5)"],
        [210,0,410,150,"rgba(66, 139, 202, 0.5)"],
        [210,150,410,0,"rgba(202, 66, 115, 0.5)"],
        [210,150,410,0,"rgba(42, 99, 221, 0.5)"],
        [210,0,410,150,"rgba(66, 139, 202, 0.5)"],
        [310,150,410,0,"rgba(66, 202, 142, 0.5)"],
        [310,150,410,0,"rgba(66, 202, 142, 0.5)"],
        [310,150,410,0,"rgba(66, 202, 142, 0.5)"]
    ];
    //Create SVG element
    var svg = d3.select("#main")
            .append("svg")
            .attr("width", w)
            .attr("height", h);

    var scale = d3.scale.linear()
            .domain([0,100])
            .range([0,290]);

    var xAxis = d3.svg.axis()
            .scale(scale)
            .orient("right");

    svg.selectAll("line")
            .data(dataset)
            .enter()
            .append("line")
            .attr("x1", function(d){return scale(d[0]);})
            .attr("y1", function(d){return scale(d[1]);})
            .attr("x2", function(d){return scale(d[2]);})
            .attr("y2", function(d){return scale(d[3]);})
            .attr("stroke",function(d){return d[4];});

    svg.selectAll("text")
            .data(dataset)
            .enter()
            .append("text")
            .text(function(d){return d[5];})
            .attr("x",function(d){return scale(d[0])+10;})
            .attr("y",function(d){return scale(d[1])+11;});

    //    svg.append("g")
    //            .attr("class", "axis")  //Assign "axis" class
    //            .call(xAxis);

    //表格开始
    function getDataSet() {
        var dataSet;
        $.ajax({
            url: basePath + "charts/picture/doApriori",
            dataType:'json',
            type:"POST",
            async: false,
            success:function(returnData) {
                dataSet = returnData;
            },
            error:doError

        });
        return dataSet;
    }
    $(document).ready(function () {
        $('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
        var table = $('#example').DataTable({
                    "bLengthChange": false, //改变每页显示数据数量
//                    "bFilter": false, //过滤功能
                    "data": getDataSet(),
                    "columns": [
                        { "title": "关联规则" },
                        { "title": "置信度" }
                    ]}
        );
    });

</script>
</body>
</html>