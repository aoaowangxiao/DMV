<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-6-6
  Time: 下午2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="../includes/common.jsp" %>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>可视化</title>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="/dmv/img/logo/logo2.png" alt="logo"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
</nav>
<div class="jumbotron" style="height: 45%;">
    <h1>可视化</h1>
    <p>请在下方选择可视化类型：</p>
</div>
<div class="page-container">
    <div class="row">
        <div class="col-md-6">
            <h2><i class=" icon-bar-chart"></i> 数据可视化</h2><br/><br/>
            <div class="block-btn">
                <a href="guide/scatter" style="margin-right: 10px;" class="button button-rounded button-flat-primary button-large"> 散点图</a>
                <a href="guide/bar" style="margin-right: 10px;" class="button button-rounded button-flat-action button-large"> 柱形图</a>
                <a href="guide/line" style="margin-right: 10px;" class="button button-rounded button-flat-caution button-large"> 折线图</a>
                <a href="guide/scatter3D" style="margin-right: 10px;" class="button button-rounded button-flat-royal button-large"> 三维散点图</a>
            </div>
        </div>
        <div class="col-md-6">
            <h2><i class="icon-eye-open"></i> 数据挖掘可视化</h2><br/><br/>
            <div class="block-btn">
                <a href="guide/kmeans" style="margin-right: 10px;" class="button button-rounded button-flat-primary button-large"> 聚类</a>
                <a href="guide/tree" style="margin-right: 10px;" class="button button-rounded button-flat-action button-large"> 分类</a>
                <a href="guide/apriori" style="margin-right: 10px;" class="button button-rounded button-flat-caution button-large"> 关联规则</a>
                <a href="guide/net" style="margin-right: 10px;" class="button button-rounded button-flat-royal button-large"> 社交网络</a>
            </div>
        </div>
    </div>
</div>
<div class="page-footer" style="position: fixed;">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>

<script type="text/javascript">
    $(function () {
        $("#popComment").popover();
    });
</script>
</body>
</html>
