<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-6-6
  Time: 下午4:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>折线图</title>
    <%@include file="../includes/common.jsp" %>
</head>
<body>
<%@include file="../includes/guideMenu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        折线图<small>显示折线图</small>
        <a href="guide/visual" style="float: right" class="button button-flat"><i class="icon-circle-arrow-left"></i> 返回</a>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">向导模式</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="guide/visual">可视化</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">折线图</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 折线图</h3>
        </div>
        <div class="panel-body">
            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="main" style="height:600px;margin-top: 20px;"></div>
            <div id="tryDiv"></div>
        </div>
    </div>
</div>
<div class="page-footer">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
</body>
</html>
<script type="text/javascript">
    function getAttr() {
        var attr;
        $.ajax({
            url: basePath + "charts/bar/getBarAttr",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnAttr) {
                attr = returnAttr;
            },
            error: doError
        });
        return attr;
    }

    function getData() {
        var finalData;
        $.ajax({
            url: basePath + "charts/bar/getBarData",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (returnAttr) {
                finalData = returnAttr;
            },
            error: doError
        });
        return finalData;
    }

    function buildData(returnData) {
        var data = [];
        for (var i = 0; i < returnData.length; i++) {
            data[i] = { name: i + 1,
                type: 'line',
                data: returnData[i]
            }

        }
        return data;
    }
    // 路径配置
    require.config({
        paths: {
            'echarts': './js/common/echarts/echarts-original',
            'echarts/chart/scatter': './js/common/echarts/echarts-original'
        }
    });
    require(
            [
                'echarts',
                'echarts/chart/bar',
                'echarts/chart/line'
            ],
            function (ec) {
                //--- 折柱 ---
                var myChart = ec.init(document.getElementById('main'));
                myChart.setOption({
                    tooltip: {
                        trigger: 'axis'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            mark: {show: true},
                            dataView: {show: true, readOnly: false},
                            magicType: {show: true, type: ['line', 'bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            data: getAttr()
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            splitArea: {show: true}
                        }
                    ],
                    series:buildData(getData())
                });


            }
    );
</script>