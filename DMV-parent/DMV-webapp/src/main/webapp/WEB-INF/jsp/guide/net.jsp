<%--
  Created by IntelliJ IDEA.
  User: Sky
  Date: 14-8-20
  Time: 下午9:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>社交网络</title>
    <%@include file="../includes/common.jsp" %>
</head>
<body>
<%@ include file="../includes/menu.jsp" %>
<div class="container" style="padding: 0 20px;">
    <h3 class="page-title">
        社交网络
        <small>社交网络构建</small>
        <a href="guide/visual" style="float: right" class="button button-flat"><i class="icon-circle-arrow-left"></i> 返回</a>
    </h3>
    <ol class="breadcrumb">
        <li><i class="fa icon-home"></i><a href="#">主页</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li><a href="charts/scatter/net">数据挖掘</a></li>
        <li><i class="fa icon-angle-right"></i></li>
        <li class="font-12">社交网络</li>
    </ol>
    <div class="panel panel-primary" style="float:right;width: 100%;">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-bar-chart"></i> 社交网络（网状图）</h3>

            <div class="btn-group" style="float: right;margin-top: -25px;">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                    构建社交网络 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a data-toggle="modal" data-target="#addNode">添加节点</a></li>
                    <li><a data-toggle="modal" data-target="#addRel">添加关系</a></li>
                </ul>
            </div>

        </div>
        <div class="panel-body">

            <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
            <div id="wang" style="height:500px;margin-top: 20px;"></div>

            <!-- Modal -->
            <div class="modal fade" id="addNode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="addNodeLabel">添加节点</h4>
                        </div>
                        <div class="modal-body">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="text" class="form-control" placeholder="节点名称" id="nodeName">
                            </div>
                            </br>
                        </div>
                        <div class="modal-footer" id="addNodeBtn">
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <button type="button" class="btn btn-primary" onclick="addNewNode()" id="addSure">确定
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- Modal -->
            <div class="modal fade" id="addRel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="addRelLabel">添加关系</h4>
                        </div>
                        <div class="modal-body">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="text" class="form-control" placeholder="起始节点" id="qName">
                            </div>
                            <br/>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-screenshot"></i></span>
                                <input type="text" class="form-control" placeholder="目标节点" id="mName">
                            </div>
                            <br/>


                        </div>
                        <div class="modal-footer" id="addRelBtn">
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <button type="button" class="btn btn-primary" onclick="addNewLink()" id="addNewLink">确定
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
    var node = [];
    var link = [];
    var ni = 0;
    var li = 0;
    var na = 0;
    var noName = [];

    function addNewNode() {
        var nodeName = $("#nodeName").val();
        if (na == 0) {
            buildNode(nodeName);
            noName[na] = nodeName;
            na++;
        } else {
            for (var i = 0; i < na; i++) {
                if (nodeName == noName[i]) {
                    alert("节点重名!");
                    return;
                }
            }
            buildNode(nodeName);
            noName[na] = nodeName;
            na++;
        }
        var option = rebuildOption();
        initChart(option);
        $("#addSure").attr("data-dismiss", "modal");
    }

    function addNewLink() {
        var sName = $("#qName").val();
        var tName = $("#mName").val();
        buildLink(sName, tName);
        var option = rebuildOption();
        initChart(option);
        $("#addNewLink").attr("data-dismiss", "modal");
    }


    function initChart(option) {
        require.config({
            paths: {
                'echarts': './js/common/echarts/echarts',
                'echarts/chart/force': './js/common/echarts/echarts'
            }
        });
        require(
                [
                    'echarts',
                    'echarts/chart/force'
                ],
                function (ec) {
                    myChart = ec.init(document.getElementById("wang"));

                    myChart.setOption(option);
                }
        );
    }

    function buildNode(nodeName) {
        node[ni] = {category: 0, name: nodeName, value: 10};
        ni++;

        return node;
    }

    function buildLink(sourceName, targetName) {
        link[li] = {source: sourceName, target: targetName, weight: 1};
        li++;
        return link;
    }

    function rebuildOption() {
        option = {
            tooltip: {
                trigger: 'item',
                formatter: '{a}'
            },
            toolbox: {
                show: true,
                feature: {
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            legend: {
                x: 'left',
                data: []
            },
            series: [
                {
                    type: 'force',
                    name: " ",
                    categories: [
                        {
                            name: ' '
                        }
                    ],
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    color: '#333'
                                }
                            },
                            nodeStyle: {
                                brushType: 'both',
                                strokeColor: 'rgba(255,215,0,0.4)',
                                lineWidth: 1
                            }
                        },
                        emphasis: {
                            label: {
                                show: false
                                // textStyle: null      // 默认使用全局文本样式，详见TEXTSTYLE
                            },
                            nodeStyle: {
                                //r: 30
                            },
                            linkStyle: {}
                        }
                    },
                    useWorker: false,
                    minRadius: 15,
                    maxRadius: 25,
                    gravity: 1.1,
                    scaling: 1.1,
                    linkSymbol: 'arrow',
                    nodes: node,
                    links: link
                }
            ]
        };
        return option;
    }

    $(function () {
        initChart(option);
    })

</script>