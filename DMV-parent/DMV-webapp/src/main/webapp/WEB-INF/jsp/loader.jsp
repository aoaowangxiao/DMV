<%--
  Created by IntelliJ IDEA.
  User: wangxiao
  Date: 14-5-8
  Time: 下午7:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="./includes/common.jsp" %>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>数据导入</title>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="/dmv/img/logo/logo2.png" alt="logo"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
</nav>
<div class="jumbotron" style="height: 45%;">
    <h1>您好！</h1>
    <p>欢迎使用DMV数据挖掘可视化软件，请从下列两种方式中选择一种导入数据：</p>
</div>
<div class="page-container">
    <div class="row">
        <div class="col-md-6">
            <h2><i class="icon-file-alt"></i> 从文件导入</h2>

            <p>被支持的文件格式有：<span class="label label-primary">arff</span> <span class="label label-primary">csv</span>
                ，上传其他格式文件会报错。</p>

            <form class="up-form" name="upLoad" id="upLoad" enctype="multipart/form-data" method="post">
                <input class="up-input" type="file" name="file" value="" id="file"/>
            </form>
            <input type="button" class="button button-primary" onclick="upFile()" value="上传文件"/>
        </div>
        <div class="col-md-6">
            <h2><i class="icon-hdd"></i> 连接数据库</h2>

            <p>从远程数据库中选取所需数据。</p>

            <p>用户需要提供 <span class="label label-primary">数据库地址</span> <span class="label label-primary">用户名</span> <span
                    class="label label-primary">密码</span> 以及 <span class="label label-primary">端口号</span></p>

            <p>连接成功后需手动选择数据表。</p>
            <!-- Button trigger modal -->
            <button class="button button-primary" data-toggle="modal" data-target="#myModal">编辑连接</button>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">编辑连接</h4>
                        </div>
                        <div class="modal-body">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-link"></i></span>
                                <input type="text" class="form-control" placeholder="数据库地址">
                            </div>
                            </br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="text" class="form-control" placeholder="用户名">
                            </div>
                            </br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input type="password" class="form-control" placeholder="密码">
                            </div>
                            </br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-screenshot"></i></span>
                                <input type="text" class="form-control" placeholder="端口号">
                            </div>
                        </div>
                        <div class="modal-footer" id="link">
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <button type="button" class="btn btn-primary" onclick="linkok()">连接</button>
                        </div>
                        <div class="modal-footer" style="display: none;" id="select-table">
                            <div class="select-table">
                            <label style="float: left;">选择数据表</label>
                            <select style="float: left;margin-left: 15px;margin-top: 5px;">
                                <option value="double">table1</option>
                                <option value="int">KmeansData</option>
                            </select>
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel()">取消</button>
                            <button type="button" class="btn btn-primary">选择</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </div>
</div>
<div class="page-footer" style="position: fixed;">
    2014 © DataMiningVisualization by Premiere Team.
    <span id="back-to-top" class="go-top">
        <i class="fa icon-angle-up"></i>
    </span>
</div>
<script type="text/javascript">
    function linkok() {
        alert("连接成功，请选择数据表！");
        document.getElementById("link").style.display="none";
        document.getElementById("select-table").style.display="";
    }
    function cancel() {
        document.getElementById("link").style.display="";
        document.getElementById("select-table").style.display="none";
    }

    function upFile() {
        var fileName;
        if (window.navigator.userAgent.indexOf("Firefox") != -1) {
            var tempName = $("#file").val();
            var lastName = tempName.substr(tempName.length - 3, 3);
            if (lastName != "csv" && lastName != "xml") {
                lastName = tempName.substr(tempName.length - 4, 4);
                if (lastName != "arff" && lastName != "pmml") {
                    lastName = tempName.substr(tempName.length - 5, 5);
                    if (lastName != "excel") {
                        alert("文件格式选择错误！请重新选择！");
                        return;
                    }
                }
            }
            fileName = $("#file").val();
        } else {
            var tempName = $("#file").val();
            var lastName = tempName.substr(tempName.length - 3, 3);
            if (lastName != "csv" && lastName != "xml") {
                lastName = tempName.substr(tempName.length - 4, 4);
                if (lastName != "arff" && lastName != "pmml") {
                    lastName = tempName.substr(tempName.length - 5, 5);
                    if (lastName != "excel") {
                        alert("文件格式选择错误！请重新选择！")
                        return;
                    }
                }
            }
            fileName = "datav." + lastName;
        }
        $("#upLoad").attr("action", basePath + "loader/upData?fileName=" + fileName).submit();
    }
    function test() {
        $.ajax({
            url: basePath + "loader/test",
            type: "post",
            dataType: "json",
            success: function () {
            },
            error: doError
        });
    }

</script>
</body>
</html>
