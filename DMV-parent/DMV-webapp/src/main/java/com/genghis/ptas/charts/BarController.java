package com.genghis.ptas.charts;

import com.genghis.ptas.charts.scatter.service.BarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-5-21
 * Time: 下午9:20
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/charts/bar/")
public class BarController {
    @Autowired
    private BarService barService;

    @RequestMapping(value = "showBar", method = {RequestMethod.GET})
    public String showBar(HttpServletRequest request) {
        return "charts/bar/showBar";
    }

    @RequestMapping(value = "getBarAttr", method = {RequestMethod.POST})
    @ResponseBody
    public List<String> getBarAttr() {
        return barService.getBarAttr();
    }

    @RequestMapping(value = "getBarData", method = {RequestMethod.POST})
    @ResponseBody
    public List<List<Double>> getBarData() {
        return barService.getBarData();
    }
}
