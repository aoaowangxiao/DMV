package com.genghis.ptas.data;

import com.genghis.ptas.clean.service.DataCleanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wangxiao
 * Date: 14-5-16
 * Time: 下午8:33
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/data/")
public class DataCleanController {

    @Autowired
    private DataCleanService dataCleanService;

    @RequestMapping(value = "clean", method = {RequestMethod.GET})
    public String clean() {
        return "data/clean";
    }

    @RequestMapping(value = "guideClean", method = {RequestMethod.GET})
    public String guideClean() {
        return "data/guideClean";
    }

    @RequestMapping(value = "overrideDataSet", method = {RequestMethod.POST})
    @ResponseBody
    public String overrideDataSet(@RequestParam("dataSet") List<String> dataSet, @RequestParam("attr") List<String> attr) {
        List<String> list = dataCleanService.getDataSet(dataSet, attr);
        dataCleanService.insertDataSet(list, attr);
        return "1";
    }

    @RequestMapping(value = "del", method = {RequestMethod.POST})
    @ResponseBody
    public String del(@RequestParam("deleteAttr") List<String> deleteAttr) {
        dataCleanService.deleteClums(deleteAttr);
        return "1";
    }
}
