package com.genghis.ptas.charts;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-5-21
 * Time: 下午9:20
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/charts/line/")
public class LineController {
    @RequestMapping(value = "showLine",method = {RequestMethod.GET})
    public String showLine() {
        return "charts/line/showLine";
    }
    @RequestMapping(value = "aprioriParallel",method = {RequestMethod.GET})
    public String aprioriParallel() {
        return "charts/line/aprioriParallel";
    }
}
