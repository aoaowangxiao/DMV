package com.genghis.ptas.data;

import com.genghis.core.weka.WekaFactory;
import com.genghis.ptas.data.service.DataTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wangxiao
 * Date: 14-5-16
 * Time: 下午8:33
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/data/")
public class DataTableController {
    @Autowired
    private DataTableService dataTableService;


    @RequestMapping(value = "table", method = {RequestMethod.GET})
    public String table() {
        return "data/table";
    }

    @RequestMapping(value = "getAttr", method = {RequestMethod.POST})
    @ResponseBody
    public List<String> getAttr() {
        return dataTableService.getAttr();
    }

    @RequestMapping(value = "getdata", method = {RequestMethod.POST})
    @ResponseBody
    public List<List<String>> getData() {
        return dataTableService.getData();
    }

}
