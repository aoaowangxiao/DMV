package com.genghis.ptas.charts;

import com.genghis.ptas.charts.scatter.entity.ScatterPoint;
import com.genghis.ptas.charts.scatter.service.ScatterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-15
 * Time: 下午2:59
 * To change this template use File | Settings | File Templates.
 */

@Controller
@Transactional
@RequestMapping("/charts/scatter/")

public class ScatterController {

    @Autowired
    private ScatterService scatterService;

    @RequestMapping(value = "showScatter", method = {RequestMethod.GET})
    public String showScatter() {
        return "charts/scatter/showScatter";
    }

    @RequestMapping(value = "showScatter3D", method = {RequestMethod.GET})
    public String showScatter3D() {
        return "charts/scatter/showScatter3D";
    }

    @RequestMapping(value = "kmeansParallel", method = {RequestMethod.GET})
    public String kmeansParallel() {
        return "charts/scatter/kmeansParallel";
    }

    @RequestMapping(value = "net", method = {RequestMethod.GET})
        public String net() {
            return "charts/scatter/net";
        }

    @RequestMapping(value = "kmeansScatter", method = {RequestMethod.GET})
    public String kmeansScatter() {
        return "charts/scatter/kmeansScatter";
    }

    @RequestMapping(value = "kmeansRadar", method = {RequestMethod.GET})
    public String kmeansRadar() {
        return "charts/scatter/kmeansRadar";
    }

    @RequestMapping(value = "doKMeans", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doKMeans() {
        return scatterService.getKMeansData();
    }

    @RequestMapping(value = "getPoint", method = RequestMethod.POST)
    @ResponseBody
    public List<List<Double>> getScatterValue() {
        List<Map<String, Object>> list = scatterService.getDatabaseData();
        return scatterService.getScatterData(list);
    }
}
