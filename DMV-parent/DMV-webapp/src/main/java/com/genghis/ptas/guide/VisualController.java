package com.genghis.ptas.guide;

import com.genghis.ptas.charts.scatter.service.ScatterService;
import com.genghis.ptas.clean.service.DataCleanService;
import com.genghis.ptas.loader.service.FileLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import weka.core.Instances;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-6-6
 * Time: 下午2:59
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/guide/")
public class VisualController {

    @Autowired
    private DataCleanService dataCleanService;

    @Autowired
    private FileLoaderService fileLoaderService;

    @Autowired
    private ScatterService scatterService;

    @RequestMapping(value = "visual", method = {RequestMethod.GET})
    public String visual() {
        return "guide/visual";
    }

    @RequestMapping(value = "clean", method = {RequestMethod.POST})
    @ResponseBody
    public String guideClean() throws Exception {
        Instances instances = dataCleanService.getInstances();
        Instances newData = dataCleanService.useFilter(instances);
        fileLoaderService.instance2db(newData);
        return "1";
    }

    @RequestMapping(value = "scatter", method = {RequestMethod.GET})
    public String scatter() {
        return "guide/scatter";
    }

    @RequestMapping(value = "scatter3D", method = {RequestMethod.GET})
    public String scatter3D() {
        return "guide/scatter3D";
    }

    @RequestMapping(value = "bar", method = {RequestMethod.GET})
    public String bar() {
        return "guide/bar";
    }

    @RequestMapping(value = "line", method = {RequestMethod.GET})
    public String line() {
        return "guide/line";
    }

    @RequestMapping(value = "kmeans", method = {RequestMethod.GET})
    public String kmeans() {
        return "guide/kmeans";
    }

    @RequestMapping(value = "tree", method = {RequestMethod.GET})
    public String tree() {
        return "guide/tree";
    }

    @RequestMapping(value = "apriori", method = {RequestMethod.GET})
    public String apriori() {
        return "guide/apriori";
    }

    @RequestMapping(value = "kmeansParallel", method = {RequestMethod.GET})
    public String kmeansParallel() {
        return "guide/kmeansParallel";
    }

    @RequestMapping(value = "net", method = {RequestMethod.GET})
    public String net() {
        return "guide/net";
    }

    @RequestMapping(value = "kmeansRadar", method = {RequestMethod.GET})
    public String kmeansRadar() {
        return "guide/kmeansRadar";
    }

//    @RequestMapping(value = "getScatterData", method = {RequestMethod.POST})
//    @ResponseBody
//    public List<Map<String, Object>> getScatterData() {
//        List<Map<String, Object>> list = scatterService.getData();
//        return list;
//    }
}
