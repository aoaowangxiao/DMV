package com.genghis.ptas.charts;

import com.genghis.ptas.data.service.TreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import weka.classifiers.Classifier;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-5-21
 * Time: 下午9:20
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/charts/tree/")
public class TreeController {
    @Autowired
    private TreeService treeService;

    @RequestMapping(value = "c45tree",method = {RequestMethod.GET})
    public String c45tree() {
        return "charts/tree/c45tree";
    }

    @RequestMapping(value = "getResult", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, String>> getTreeData() {
        return treeService.getResultMap(treeService.getNeedString());
    }

//    @RequestMapping(value = "getTreeValue",method = {RequestMethod.GET})
//    public Classifier getInstances() {
//       return treeService.getNeedString();
//    }
}
