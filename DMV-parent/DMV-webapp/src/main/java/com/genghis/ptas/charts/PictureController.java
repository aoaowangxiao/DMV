package com.genghis.ptas.charts;

import com.genghis.ptas.charts.scatter.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-5-21
 * Time: 下午9:20
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Transactional
@RequestMapping("/charts/picture/")
public class PictureController {

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "aprioriPicture", method = {RequestMethod.POST,RequestMethod.GET})
     public String showApriori() {
        return "charts/picture/aprioriPicture";
    }

    @RequestMapping(value = "J48Picture", method = {RequestMethod.POST,RequestMethod.GET})
    public String showJ48() {
        return "charts/picture/J48Picture";
    }

    @RequestMapping(value = "doApriori", method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public List<List<String>> doApriori() {
        return pictureService.getApriori();
    }

    @RequestMapping(value = "doJ48", method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public String doJ48() {
        return pictureService.getJ48();
    }
}
