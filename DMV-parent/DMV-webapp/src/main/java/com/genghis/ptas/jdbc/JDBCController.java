/**
 * Created with IntelliJ IDEA.
 * User: panzhuowen
 * Date: 14-7-19
 * Time: 下午1:50
 * To change this template use File | Settings | File Templates.
 */
package com.genghis.ptas.jdbc;

import com.genghis.ptas.jdbc.entity.JDBCInfo;
import com.genghis.ptas.jdbc.service.JDBCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Connection;
import java.util.List;

/**
 * 类的描述信息
 *
 * @version 1.0.1
 * @Author panzhuowen
 */
@Controller
@Transactional
@RequestMapping("/jdbc/")
public class JDBCController {

    @Autowired
    private JDBCService jdbcService;

    @RequestMapping(value = "mysql", method = RequestMethod.POST)
    @ResponseBody
    public List<String> getMySqlDatabases(JDBCInfo jdbcInfo) {
        Connection conn = jdbcService.getConnectMysqlDatabase(jdbcInfo);
        return jdbcService.getAllDatabases(conn);
    }

    @RequestMapping(value = "sqlserver", method = RequestMethod.POST)
    @ResponseBody
    public List<String> getSqlServerDatabases(JDBCInfo jdbcInfo) {
        Connection conn = jdbcService.getSqkServerConnectionDatabase(jdbcInfo);
        return jdbcService.getAllDatabases(conn);
    }

    @RequestMapping(value = "mysqltables", method = RequestMethod.POST)
    @ResponseBody
    public List<String> getMySqlTables(JDBCInfo jdbcInfo) {
        Connection conn = jdbcService.getMySqlConnectionTable(jdbcInfo);
        return jdbcService.getAllTables(conn);
    }

    @RequestMapping(value = "sqlservertables", method = RequestMethod.POST)
    @ResponseBody
    public List<String> getSqlServerTables(JDBCInfo jdbcInfo) {
        Connection conn = jdbcService.getSqlServerConnectionTable(jdbcInfo);
        return jdbcService.getAllTables(conn);
    }

    @RequestMapping(value = "mysqlcopy" ,method = RequestMethod.POST)
    @ResponseBody
    public String getMySqlData(JDBCInfo jdbcInfo) {
        Connection connection = jdbcService.getMySqlConnectionTable(jdbcInfo);
        jdbcService.copyTowTable(connection,jdbcInfo.getTableName());
        return "0";
    }

    @RequestMapping(value = "sqlservercopy" ,method = RequestMethod.POST)
    @ResponseBody
    public String getSqlServerData(JDBCInfo jdbcInfo) {
        Connection connection = jdbcService.getSqlServerConnectionTable(jdbcInfo);
        jdbcService.copyTowTable(connection,jdbcInfo.getTableName());
        return "0";
    }

    @RequestMapping(value = "mysql" ,method = RequestMethod.GET)
     public String getMySqlPage() {
        return "loader/mysql";
    }

    @RequestMapping(value = "sqlserver" ,method = RequestMethod.GET)
    public String getSqlServerPage() {
        return "loader/sqlserver";
    }

    @RequestMapping(value = "oracle" ,method = RequestMethod.GET)
    public String getOraclePage() {
        return "loader/oracle";
    }

    @RequestMapping(value = "guidemysql" ,method = RequestMethod.GET)
    public String getGuideMySqlPage() {
        return "loader/guidemysql";
    }

    @RequestMapping(value = "guidesqlserver" ,method = RequestMethod.GET)
    public String getGuideSqlServerPage() {
        return "loader/guidesqlserver";
    }

    @RequestMapping(value = "guideoracle" ,method = RequestMethod.GET)
    public String getGuideOraclePage() {
        return "loader/guideoracle";
    }
}
