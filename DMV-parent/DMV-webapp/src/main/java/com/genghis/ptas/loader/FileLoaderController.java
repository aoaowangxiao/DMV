package com.genghis.ptas.loader;

import com.genghis.core.weka.WekaFactory;
import com.genghis.ptas.loader.service.FileLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import weka.core.Instances;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* Created with IntelliJ IDEA.
* User: Administrator
* Date: 14-5-8
* Time: 下午7:29
* To change this template use File | Settings | File Templates.
*/
@Controller
@Transactional
@RequestMapping("/loader/")
public class FileLoaderController {
    @Autowired
    private FileLoaderService fileLoaderService;

    @RequestMapping(value = "fileLoader", method = {RequestMethod.GET})
    public String fileLoader() {
        return "loader/fileLoader";
    }

    @RequestMapping(value = "guideFileLoader", method = {RequestMethod.GET})
    public String guideFileLoader() {
        return "loader/guideFileLoader";
    }

    @RequestMapping(value = "upData", method = {RequestMethod.POST})
    public String upFile(@RequestParam("fileName") String fileName,HttpServletRequest request) throws Exception {
        String filePath = fileLoaderService.upFile(request, "file", fileName);
        filePath = filePath + fileName;
        Instances instances = fileLoaderService.fileLoader(filePath);
        fileLoaderService.instance2db(instances);
        WekaFactory wekaFactory = new WekaFactory();
        String list = wekaFactory.getDataSet(wekaFactory.instance2data(instances));
        request.setAttribute("dataList",list);
        return "data/table";
    }

    @RequestMapping(value = "upDataGuide", method = {RequestMethod.POST})
    public String upDataGuide(@RequestParam("fileName") String fileName,HttpServletRequest request) throws Exception {
        String filePath = fileLoaderService.upFile(request, "file", fileName);
        filePath = filePath + fileName;
        Instances instances = fileLoaderService.fileLoader(filePath);
        fileLoaderService.instance2db(instances);
        return "data/guideClean";
    }
}

