package com.genghis.ptas.charts.scatter.entity;

/**
 * Created with IntelliJ IDEA.
 * User: Surc
 * Date: 14-4-15
 * Time: 下午4:35
 * To change this template use File | Settings | File Templates.
 */
public class ScatterPoint {
    double height, weight;
    int name;

    public ScatterPoint() {
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }
}
