/*
* BarServiceImpl.java
* Created on  2014-6-6 下午12:37
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-6-6       panzhuowen    初始版本
*
*/
package com.genghis.ptas.charts.scatter.service.impl;

import com.genghis.core.weka.WekaFactory;
import com.genghis.ptas.charts.scatter.dao.BarDao;
import com.genghis.ptas.charts.scatter.service.BarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
@Service("barService")
public class BarServiceImpl implements BarService {
    @Autowired
    private BarDao barDao;

    @Override
    public List<String> getNeedAttr() {
        Instances instances = barDao.getInstances();
        WekaFactory wekaFactory = new WekaFactory();
        List<String> attrs = wekaFactory.instance2attribute(instances);
        int[] index = wekaFactory.getIndexForNotNum(attrs);
        wekaFactory.rebuildAttr(attrs, index);
        return attrs;
    }

    @Override
    public List<String> getNeedData() {
        Instances instances = barDao.getInstances();
        WekaFactory wekaFactory = new WekaFactory();
        List<String> data = wekaFactory.instance2data(instances);
        List<String> attr = wekaFactory.instance2attribute(instances);
        int[] index = wekaFactory.getIndexForNotNum(attr);
        return wekaFactory.rebuildData(data, index);
    }

    @Override
    public boolean isCanPointer(List<Map<String, Object>> list) {

        return false;
    }

    @Override
    public List<String> getBarAttr() {
        List<Map<String, Object>> list = barDao.getBarData();
        return getAttr(list);
    }

    @Override
    public List<List<Double>> getBarData() {
        List<Map<String, Object>> list = barDao.getBarData();
        String cl = getclum(getAttr(list));
        List<Map<String, Object>> FList = barDao.getFinalData(cl);
        List<List<Double>> returnList = new ArrayList<List<Double>>();
        for (Map<String, Object> map : FList) {
            List<Double> temp = new ArrayList<Double>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                temp.add(Double.parseDouble(entry.getValue().toString()));
            }
            returnList.add(temp);
        }
        return returnList;
    }

    private List<String> getAttr(List<Map<String, Object>> data) {
        List<String> attr = new ArrayList<String>();
        Map<String, Object> map = data.get(0);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String val = entry.getValue().toString();
            if (val.matches("[\\d.]+")) {
                attr.add(entry.getKey());
            }
        }

        return attr;
    }

    private String getclum(List<String> attr) {
        String clums = "";
        for (String str : attr) {
            clums = clums + str + ",";
        }
        return clums.substring(0, clums.length() - 1);
    }
}
