/**
 * Created with IntelliJ IDEA.
 * User: panzhuowen
 * Date: 14-7-19
 * Time: 下午1:58
 * To change this template use File | Settings | File Templates.
 */
package com.genghis.ptas.jdbc.dao.impl;

import com.genghis.core.weka.WekaFactory;
import com.genghis.ptas.jdbc.dao.JDBCDao;
import com.genghis.ptas.util.JDBCTools;
import com.genghis.ptas.util.JDBCUtilDao;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 类的描述信息
 *
 * @version 1.0.1
 * @Author panzhuowen
 */
@Repository("jDBCDao")
public class JDBCDaoImpl extends WekaFactory implements JDBCDao{
    @Override
    public Connection getConnection(String url, Properties properties,String driver) {
        return JDBCTools.getConnection(url,properties,driver);
    }

    @Override
    public List<Map<String, Object>> getOtherData(Connection connection,String tableName) {
        JDBCUtilDao jdbcUtilDao = new JDBCUtilDao();
        return jdbcUtilDao.queryForList(connection,tableName);
    }

    @Override
    public void createTable(String clums) {
        String drop = "DROP TABLE IF EXISTS data_temp";
        getJdbcTemplate().update(drop);
        String sql = "CREATE TABLE data_temp (" + clums + ")";
        getJdbcTemplate().update(sql);
    }

    @Override
    public void insertData(String cl, String data) {
        String sql ="INSERT INTO data_temp (" + cl + ")" + " VALUES(" + data + ")";
        getJdbcTemplate().update(sql);
    }
}
