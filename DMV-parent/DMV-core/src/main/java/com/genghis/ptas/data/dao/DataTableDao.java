/*
* DataTableDao.java
* Created on  2014-5-25 下午3:40
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-25       panzhuowen    初始版本
*
*/
package com.genghis.ptas.data.dao;

import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface DataTableDao {

    Instances getInstances();

    List<Map<String, Object>> getData();

}
