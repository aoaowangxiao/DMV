package com.genghis.ptas.clean.dao.impl;

import com.genghis.core.weka.WekaBaseDao;
import com.genghis.ptas.clean.dao.DataCleanDao;
import org.springframework.stereotype.Service;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: wangxiao
 * Date: 14-5-25
 * Time: 下午7:01
 * To change this template use File | Settings | File Templates.
 */
@Service("DataCleanDao")
public class DataCleanDdaoImpl extends WekaBaseDao implements DataCleanDao {
    @Override
    public List<Map<String, Object>> db2MapList() {
        String sql = "SELECT * FROM data_temp";
        return getJdbcTemplate().queryForList(sql);
    }

    @Override
    public Instances getInstances() {
        String sql = "SELECT * FROM data_temp";
        return queryForInstancesWithMySQL(sql);
    }

    @Override
    public void insertDataSet(String data, String clums) {
        String sql = "INSERT INTO data_temp (" + clums + ")" + " VALUES(" + data + ")";
        getJdbcTemplate().update(sql);
    }

    @Override
    public void cleanTable() {
        String sql = "TRUNCATE TABLE data_temp";
        getJdbcTemplate().update(sql);
    }

    @Override
    public void dropClum(String clName) {
        String sql = "ALTER TABLE data_temp DROP " + clName;
        getJdbcTemplate().update(sql);
    }
}
