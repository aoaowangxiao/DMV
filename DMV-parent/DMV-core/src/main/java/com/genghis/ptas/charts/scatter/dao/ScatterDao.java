package com.genghis.ptas.charts.scatter.dao;

import com.genghis.ptas.charts.scatter.entity.ScatterPoint;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Surc
 * Date: 14-4-15
 * Time: 下午5:33
 * To change this template use File | Settings | File Templates.
 */
public interface ScatterDao {

    Object[] getKMeansDataFromDb(String clm);

    List<Map<String, Object>> getData();

    List<Map<String, Object>> getFinalData(String clums);

    Instances getInstances();
}
