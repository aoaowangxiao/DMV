/*
* BarDao.java
* Created on  2014-6-6 下午12:35
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-6-6       panzhuowen    初始版本
*
*/
package com.genghis.ptas.charts.scatter.dao;

import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface BarDao {

    Instances getInstances();

    List<Map<String, Object>> getBarData();

    List<Map<String, Object>> getFinalData(String clums);
}
