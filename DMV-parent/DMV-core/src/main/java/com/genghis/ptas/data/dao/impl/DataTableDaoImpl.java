/*
* DataTableDaoImpl.java
* Created on  2014-5-25 下午3:41
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-25       panzhuowen    初始版本
*
*/
package com.genghis.ptas.data.dao.impl;

import com.genghis.core.weka.WekaBaseDao;
import com.genghis.ptas.data.dao.DataTableDao;
import org.springframework.stereotype.Service;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
@Service("dataTableDao")
public class DataTableDaoImpl extends WekaBaseDao implements DataTableDao{
    @Override
    public Instances getInstances() {
        String sql = "SELECT * FROM data_temp";
        return queryForInstancesWithMySQL (sql);
    }

    @Override
    public List<Map<String, Object>> getData() {
        String sql = "SELECT * FROM data_temp";
        return getJdbcTemplate().queryForList(sql);
    }
}
