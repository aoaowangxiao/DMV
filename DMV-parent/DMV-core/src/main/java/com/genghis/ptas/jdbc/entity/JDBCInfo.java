/**
 * Created with IntelliJ IDEA.
 * User: panzhuowen
 * Date: 14-7-19
 * Time: 下午1:30
 * To change this template use File | Settings | File Templates.
 */
package com.genghis.ptas.jdbc.entity;

import java.util.Properties;

/**
 * 类的描述信息
 *
 * @version 1.0.1
 * @Author panzhuowen
 */
public class JDBCInfo {
    private String serverName;
    private String user;
    private String password;
    private String url;
    private String databaseName;
    private String tableName;
    private int port;

    public JDBCInfo() {
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
