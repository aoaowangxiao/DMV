/*
* DataTableServiceImpl.java
* Created on  2014-5-25 下午3:40
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-25       panzhuowen    初始版本
*
*/
package com.genghis.ptas.data.service.impl;

import com.genghis.core.weka.WekaFactory;
import com.genghis.ptas.data.dao.DataTableDao;
import com.genghis.ptas.data.service.DataTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
@Service("dataTableService")
public class DataTableServiceImpl implements DataTableService {
    @Autowired
    private DataTableDao dataTableDao;

    @Override
    public List<String> getAttr() {
        List<Map<String, Object>> data = dataTableDao.getData();
        return listForAttr(data);
    }

    private List<String> listForAttr(List<Map<String, Object>> data) {
        List<String> attr = new ArrayList<String>();
        if (data.size() > 0) {
            Map<String, Object> map = data.get(0);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                attr.add(entry.getKey());
            }
        } else {
            throw new RuntimeException("数据表为空");
        }
        return attr;
    }

    @Override
    public List<List<String>> getData() {
        List<Map<String, Object>> data = dataTableDao.getData();
        return listForData(data);
    }

    private List<List<String>> listForData(List<Map<String, Object>> data) {
        List<List<String>> dataSet = new ArrayList<List<String>>();
        String[] values = null;
        if (data.size() > 0) {
            values = new String[data.get(0).size()];
            for (Map<String, Object> map : data) {
                int i = 0;
                List<String> dataSetOne = new ArrayList<String>();
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    values[i] = entry.getValue().toString();
                    i ++;
                }
                for (int j = 0;j < values.length;j ++) {
                    dataSetOne.add(values[j]);
                }
                dataSet.add(dataSetOne);
            }
        } else {
            throw new RuntimeException("数据表为空");
        }
        return dataSet;
    }
}
