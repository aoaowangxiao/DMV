package com.genghis.ptas.apriori;

//import com.genghis.core.weka.WekaBaseDao;

import weka.core.Instances;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-19
 * Time: 下午7:11
 * To change this template use File | Settings | File Templates.
 */
public class AP {
    public Map<String, String> algorithm(Instances instances){
        Apriori apriori = new Apriori();
        try{
            String[] options = new String[17];
            options[0] =  "-I";
            options[1] =  "-N";
            options[2] =  "10";         //关联结果数量
            options[3] =  "-T";
            options[4] =  "0";
            options[5] =  "-C";         //最小置信度参数
            options[6] =  "0.8";        //修改这个数据置信度
            options[7] =  "-D";         //递减迭代值
            options[8] =  "0.05";       //修改这个参数设置
            options[9] =  "-U";         //最小支持度上界参数
            options[10] =  "1.0";       //修改这个数据修改支持度
            options[11] =  "-M";        //最小支持度下界参数
            options[12] =  "0.2";       //修改这个数据修改支持度
            options[13] =  "-S";
            options[14] =  "-1.0";
            options[15] =  "-c";
            options[16] =  "-1";
            apriori.setOptions(options);
            apriori.buildAssociations(instances);

            return apriori.getResultMap();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
