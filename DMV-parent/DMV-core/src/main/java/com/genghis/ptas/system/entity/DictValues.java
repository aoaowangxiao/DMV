package com.genghis.ptas.system.entity;

/**
 * 类的描述信息
 *
 * @author chenl
 * @version 1.0.1
 */

public class DictValues {
    private String dictName;
    private String dictCode;
    private String dictValue;

    public DictValues() {
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }
}
