package com.genghis.ptas.charts.scatter.entity;

/**
 * Created with IntelliJ IDEA.
 * User: Surc
 * Date: 14-4-23
 * Time: 下午3:45
 * To change this template use File | Settings | File Templates.
 */
public class KMeansTestVo {
    private double data1, data2, data3, data4;

    public KMeansTestVo(){
    }

    public double getData1() {
        return data1;
    }

    public void setData1(double data1){
        this.data1 = data1;
    }

    public double getData2() {
        return data2;
    }

    public void setData2(double data2){
        this.data2 = data2;
    }

    public double getData3() {
        return data3;
    }

    public void setData3(double data3){
        this.data3 = data3;
    }

    public double getData4() {
        return data4;
    }

    public void setData4(double data4){
        this.data4 = data4;
    }
}
