package com.genghis.ptas.charts.scatter.entity;

/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-26
 * Time: 下午6:23
 * To change this template use File | Settings | File Templates.
 */
public class AprioriVector {
    private String antecedent;
    private String consequent;
    private double confidence;

    public AprioriVector() {
    }

    public String getAntecedent() {
        return antecedent;
    }

    public void setAntecedent(String antecedent) {
        this.antecedent = antecedent;
    }

    public String getConsequent() {
        return consequent;
    }

    public void setConsequent(String consequent) {
        this.consequent = consequent;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }
}
