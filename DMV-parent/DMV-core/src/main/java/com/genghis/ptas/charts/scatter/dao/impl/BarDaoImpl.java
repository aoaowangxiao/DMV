/*
* BarDaoImpl.java
* Created on  2014-6-6 下午12:35
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-6-6       panzhuowen    初始版本
*
*/
package com.genghis.ptas.charts.scatter.dao.impl;

import com.genghis.core.weka.WekaBaseDao;
import com.genghis.ptas.charts.scatter.dao.BarDao;
import org.springframework.stereotype.Repository;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
@Repository("barDao")
public class BarDaoImpl extends WekaBaseDao implements BarDao {
    @Override
    public Instances getInstances() {
        String sql = "SELECT * FROM data_temp";
        return queryForInstancesWithMySQL(sql);
    }

    @Override
    public List<Map<String, Object>> getBarData() {
        String sql = "SELECT * FROM data_temp";
        return getJdbcTemplate().queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> getFinalData(String clums) {
        String sql = "SELECT " + clums + " FROM data_temp";
        return getJdbcTemplate().queryForList(sql);
    }
}
