package com.genghis.ptas.charts.scatter.dao.impl;

import com.genghis.core.weka.WekaBaseDao;
import com.genghis.ptas.charts.scatter.dao.PictureDao;
import org.springframework.stereotype.Repository;
import weka.core.Instances;

/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-26
 * Time: 下午6:40
 * To change this template use File | Settings | File Templates.
 */
@Repository("pictureDao")
public class PictureDaoImpl extends WekaBaseDao implements PictureDao {
    @Override
    public Instances getInstances() {
        String sql = "select * from data_temp";
        return queryForInstancesWithMySQL(sql);  //To change body of implemented methods use File | Settings | File Templates.
    }
}
