/**
 * Created with IntelliJ IDEA.
 * User: panzhuowen
 * Date: 14-7-19
 * Time: 下午1:56
 * To change this template use File | Settings | File Templates.
 */
package com.genghis.ptas.jdbc.dao;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 类的描述信息
 *
 * @version 1.0.1
 * @Author panzhuowen
 */
public interface JDBCDao {
    Connection getConnection(String url,Properties properties,String driver);

    List<Map<String,Object>> getOtherData(Connection connection,String tableName);

    void createTable(String clums);

    void insertData(String cl,String data);
}
