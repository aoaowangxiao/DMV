package com.genghis.ptas.charts.scatter.dao;

import weka.core.Instances;

/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-26
 * Time: 下午6:40
 * To change this template use File | Settings | File Templates.
 */

public interface PictureDao {

    Instances getInstances();

}
