/*
* FileLoaderServiceImpl.java
* Created on  2014-5-8 下午7:53
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-8       panzhuowen    初始版本
*
*/
package com.genghis.ptas.loader.service.impl;


import com.genghis.ptas.loader.dao.FileLoaderDao;
import com.genghis.ptas.loader.service.FileLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
@Service("fileLoaderService")
public class FileLoaderServiceImpl implements FileLoaderService {
    @Autowired
    private FileLoaderDao fileLoaderDao;

    @Override
    public String upFile(HttpServletRequest request, String upFile, String fileName) throws ServletException, IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        return fileLoaderDao.upFile(request, upFile, fileName);
    }

    @Override
    public Instances fileLoader(String filePath) throws Exception {
        DataSource source = new DataSource(filePath);
        Instances ins = source.getDataSet();
        return ins;
    }

    @Override
    public List<String> instance2attribute(Instances ins) {
        int n = ins.numAttributes();
        Attribute[] attributes = new Attribute[n];
        List<String> att = new ArrayList<String>();
        for (int i = 0; i < n; i++) {
            attributes[i] = ins.attribute(i);
            att.add(attributes[i].name());
        }
        return att;
    }

    @Override
    public List<String> instance2data(Instances ins) {
        int n = ins.numInstances();
        List<String> data = new ArrayList<String>();
        for (int i = 0; i < n; i++) {
            data.add(ins.instance(i).toString());
        }
        return data;
    }

    @Override
    public void instance2db(Instances ins) {
        String clums = "";
        List<String> attr = instance2attribute(ins);
        for (Object obj : attr) {
            clums = clums + "`" + obj.toString() + "`" + " varchar(99),";
        }
        clums = clums.substring(0, clums.length() - 1);
        fileLoaderDao.createTable(clums, "data_temp");
        clums = "";
        for (Object obj : attr) {
            clums = clums + "`" + obj.toString() + "`" + ",";
        }
        clums = clums.substring(0, clums.length() - 1);
        for (String data : instance2data(ins)) {
            String str[] = data.split(",");
            String dataFinal = "";
            for (int i = 0; i < str.length; i++) {
                if (str[i].endsWith("'") || str[i].startsWith("'")) {
                    str[i] = str[i].replaceAll("'", "");
                }
                if (str[i].endsWith("\\")) {
                    str[i] = str[i].substring(0, str[i].length() - 1);
                }
                dataFinal = dataFinal + "'" + str[i] + "'" + ",";
            }
            dataFinal = dataFinal.substring(0, dataFinal.length() - 1);
            fileLoaderDao.insertData("data_temp", dataFinal, clums);
        }
    }

    @Override
    public List<Map<String, Object>> getMap() {
        List<Map<String, Object>> list = fileLoaderDao.getMap();
        return list;
    }

    @Override
    public Instances newInstances(List<Map<String, Object>> list) {
        FastVector attributes = new FastVector();
        int size = list.size();
        String[] key = list.get(0).keySet().toArray(new String[list.get(0).size()]);
        for (int i = 0;i < key.length;i ++) {
            attributes.addElement(key[i]);
        }
        return new Instances("WeKa",attributes,size);
    }

    @Override
    public Instances getInstances(Instances instances, List<Map<String, Object>> list) {
//        instances.attribute("data").
        return null;
    }

    @Override
    public Instances queryInstances() {
        Instances instances = fileLoaderDao.queryInstances();
        System.out.println(instances);
        return instances;
    }

    @Override
    public void insertInstance(Instances instances, String tableName) {
        fileLoaderDao.insertInstance(instances,tableName);
    }

    @Override
    public String listToString(List<String> list) {
        String s = "";
        for (String str : list) {
            s = s + str + ",";
        }
        s = s.substring(0,s.length() - 1);
        return s;
    }

    @Override
    public String changListToDataSet(List<String> list) {
        String dataSet = "";
        for (String str : list) {
            String[] strings;
            String strTemp = "";
            strings = str.split(",");
            for (int i = 0;i < strings.length; i++) {
                strTemp = strTemp + "'" + strings[i] + "',";
            }
            strTemp = strTemp.substring(0,strTemp.length() - 1);
            dataSet = dataSet + "[" + strTemp + "],";
        }
        dataSet = dataSet.substring(0,dataSet.length() - 1);
        dataSet = "[" + dataSet + "]";
        return dataSet;
    }
}
