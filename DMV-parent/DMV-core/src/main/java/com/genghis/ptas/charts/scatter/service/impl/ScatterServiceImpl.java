package com.genghis.ptas.charts.scatter.service.impl;

import com.genghis.ptas.charts.scatter.dao.ScatterDao;
import com.genghis.ptas.charts.scatter.entity.KMeansTestVo;
import com.genghis.ptas.charts.scatter.service.ScatterService;
import com.genghis.ptas.kmeans.KMeans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Surc
 * Date: 14-4-15
 * Time: 下午5:02
 * To change this template use File | Settings | File Templates.
 */
@Service("scatterService")
public class ScatterServiceImpl implements ScatterService {

    @Autowired
    private ScatterDao scatterDao;


    @Override
    public Map<String, Object> getKMeansData() {
        String clm = getKmeansclum(getAttr(scatterDao.getData()));
        Object[] kMeansData = scatterDao.getKMeansDataFromDb(clm);
        KMeans km = new KMeans();
        double[][] dataSet = changeToDouble2Array(kMeansData);
        Map<String, Object> returnMap = km.calculateForSameWeight(dataSet, 3, 1000);
        returnMap.put("Data", dataSet);
        returnMap.put("NumOfClusterer", 3);
        return returnMap;
    }

    @Override
    public List<List<Double>> getScatterData(List<Map<String, Object>> data) {
        List<String> attr = getAttr(data);
        String cl = getclum(attr);
        List<Map<String, Object>> finalData = scatterDao.getFinalData(cl);

        List<List<Double>> returnList = new ArrayList<List<Double>>();
        for (Map<String, Object> map : finalData) {
            List<Double> getData = new ArrayList<Double>();
            double[] value = new double[map.size()];
            int i = 0;
            String dataTemp = "";
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                value[i] = Double.parseDouble(entry.getValue().toString());
                i++;
            }
            for (int j = 0; j < value.length; j++) {
                getData.add(value[j]);
            }

            returnList.add(getData);
        }


        return returnList;
    }

    @Override
    public List<Map<String, Object>> getDatabaseData() {
        return scatterDao.getData();
    }


    public double[][] changeToDouble2Array(Object[] dataArr) {
        int len = dataArr.length;
        double[][] returnArr = new double[len][];
        for (int i = 0; i < len; ++i) {
            KMeansTestVo item = (KMeansTestVo) dataArr[i];
            returnArr[i] = new double[]{item.getData1(), item.getData2()};
        }
        return returnArr;
    }

    private List<String> getAttr(List<Map<String, Object>> data) {
        List<String> attr = new ArrayList<String>();
        Map<String, Object> map = data.get(0);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String val = entry.getValue().toString();
            if (val.matches("[\\d.]+")) {
                attr.add(entry.getKey());
            }
        }

        return attr;
    }

    private String getclum(List<String> attr) {
        String clums = "";
        for (String str : attr) {
            clums = clums + str + ",";
        }
        return clums.substring(0, clums.length() - 1);
    }

    private String getKmeansclum(List<String> attr) {
        String clums = "";
        int i = 1;
        for (String str : attr) {
            clums = clums + str + " data"+ i +",";
            i ++;
        }
        return clums.substring(0, clums.length() - 1);
    }

}
