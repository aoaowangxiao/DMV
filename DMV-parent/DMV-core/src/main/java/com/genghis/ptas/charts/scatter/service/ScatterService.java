package com.genghis.ptas.charts.scatter.service;

import com.genghis.ptas.charts.scatter.entity.ScatterPoint;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Surc
 * Date: 14-4-15
 * Time: 下午5:01
 * To change this template use File | Settings | File Templates.
 */
public interface ScatterService {

    Map<String, Object> getKMeansData();

    List<List<Double>> getScatterData(List<Map<String, Object>> data);

    List<Map<String, Object>> getDatabaseData();

}