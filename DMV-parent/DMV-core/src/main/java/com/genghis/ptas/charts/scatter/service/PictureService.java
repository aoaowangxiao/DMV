package com.genghis.ptas.charts.scatter.service;

import weka.core.Instances;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-26
 * Time: 下午6:08
 * To change this template use File | Settings | File Templates.
 */
public interface PictureService {
    List<List<String>> getApriori();

    String getJ48();

    Instances getInstances();
}
