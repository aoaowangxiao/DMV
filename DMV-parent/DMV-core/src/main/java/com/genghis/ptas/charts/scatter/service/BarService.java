/*
* BarService.java
* Created on  2014-6-6 下午12:36
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-6-6       panzhuowen    初始版本
*
*/
package com.genghis.ptas.charts.scatter.service;

import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface BarService {

    public List<String> getNeedAttr();

    public List<String> getNeedData();

    boolean isCanPointer(List<Map<String, Object>> list);

    public List<String> getBarAttr();

    public List<List<Double>> getBarData();

}
