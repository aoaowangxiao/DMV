/*
* FileLoaderDao.java
* Created on  2014-5-8 下午7:52
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-8       panzhuowen    初始版本
*
*/
package com.genghis.ptas.loader.dao;

import org.xml.sax.SAXException;
import weka.core.Instances;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface FileLoaderDao {

    public String upFile(HttpServletRequest request, String upFile, String fileName) throws ServletException, IOException, ParserConfigurationException, SAXException,
            XPathExpressionException;

    public void createTable(String clums, String tableName);

    public void insertData(String tableName, String data, String clums);

    public List<Map<String,Object>> getMap();

    Instances queryInstances();

    void insertInstance(Instances instances,String tableName);

}
