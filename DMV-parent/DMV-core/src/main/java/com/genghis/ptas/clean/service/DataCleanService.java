package com.genghis.ptas.clean.service;

import weka.core.Instances;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wangxiao
 * Date: 14-5-25
 * Time: 下午6:43
 * To change this template use File | Settings | File Templates.
 */
public interface DataCleanService {

    Instances getInstances();

    Instances useFilter(Instances instances) throws Exception;

    List<String> getDataSet(List<String> dataSet,List<String> attr);

    void insertDataSet(List<String> list,List<String> attr);

    void deleteClums(List<String> deleteAtte);
}
