package com.genghis.ptas.jdbc.service;

import com.genghis.ptas.jdbc.entity.JDBCInfo;

import java.sql.Connection;
import java.util.List;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface JDBCService {

    Connection getConnectMysqlDatabase(JDBCInfo jdbcInfo);

    Connection getMySqlConnectionTable(JDBCInfo jdbcInfo);

    Connection getSqkServerConnectionDatabase(JDBCInfo jdbcInfo);

    Connection getSqlServerConnectionTable(JDBCInfo jdbcInfo);

    List<String> getAllDatabases(Connection connection);

    List<String> getAllTables(Connection connection);

    void copyTowTable(Connection connection,String tableName);


}
