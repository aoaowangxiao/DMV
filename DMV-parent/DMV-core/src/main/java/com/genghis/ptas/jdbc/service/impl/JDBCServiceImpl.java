/**
 * Created with IntelliJ IDEA.
 * User: panzhuowen
 * Date: 14-7-19
 * Time: 下午1:55
 * To change this template use File | Settings | File Templates.
 */
package com.genghis.ptas.jdbc.service.impl;

import com.genghis.ptas.jdbc.dao.JDBCDao;
import com.genghis.ptas.jdbc.entity.JDBCInfo;
import com.genghis.ptas.jdbc.service.JDBCService;
import com.genghis.ptas.util.JDBCUtilDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 类的描述信息
 *
 * @version 1.0.1
 * @Author panzhuowen
 */
@Service("jDBCService")
public class JDBCServiceImpl implements JDBCService{

    @Autowired
    private JDBCDao jdbcDao;

    @Override
    public Connection getConnectMysqlDatabase(JDBCInfo jdbcInfo) {
        String url = "jdbc:mysql://" + jdbcInfo.getUrl() + ":" + jdbcInfo.getPort();
        String driver = "com.mysql.jdbc.Driver";
        Properties properties = new Properties();
        properties.put("user",jdbcInfo.getUser());
        properties.put("password",jdbcInfo.getPassword());
        return jdbcDao.getConnection(url,properties,driver);
    }

    @Override
    public Connection getMySqlConnectionTable(JDBCInfo jdbcInfo) {
        String url = "jdbc:mysql://" + jdbcInfo.getUrl() + ":" + jdbcInfo.getPort() + "/" + jdbcInfo.getDatabaseName();
        String driver = "com.mysql.jdbc.Driver";
        Properties properties = new Properties();
        properties.put("user",jdbcInfo.getUser());
        properties.put("password",jdbcInfo.getPassword());
        return jdbcDao.getConnection(url,properties,driver);
    }

    @Override
    public Connection getSqkServerConnectionDatabase(JDBCInfo jdbcInfo) {
        String url = "jdbc:sqlserver://" + jdbcInfo.getUrl() + ":1433";
        Properties properties = new Properties();
        properties.put("user",jdbcInfo.getUser());
        properties.put("password",jdbcInfo.getPassword());
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        return jdbcDao.getConnection(url,properties,driver);
    }

    @Override
    public Connection getSqlServerConnectionTable(JDBCInfo jdbcInfo) {
        String url = "jdbc:sqlserver://" + jdbcInfo.getUrl() + ":1433;databaseName=" + jdbcInfo.getDatabaseName();
        Properties properties = new Properties();
        properties.put("user",jdbcInfo.getUser());
        properties.put("password",jdbcInfo.getPassword());
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        return jdbcDao.getConnection(url,properties,driver);
    }

    @Override
    public List<String> getAllDatabases(Connection connection) {
        JDBCUtilDao jdbcUtilDao = new JDBCUtilDao();
        return jdbcUtilDao.getAllDataBase(connection);
    }

    @Override
    public List<String> getAllTables(Connection connection) {
        JDBCUtilDao jdbcUtilDao = new JDBCUtilDao();
        return jdbcUtilDao.getAllTables(connection);
    }

    @Override
    public void copyTowTable(Connection connection,String tableName) {
        List<Map<String,Object>> data = jdbcDao.getOtherData(connection, tableName);
        String cl = getColums(data);
        jdbcDao.createTable(cl);
        List<String> singleData = getData(data);
        String clums = getcl(data);
        for (String str : singleData) {
            jdbcDao.insertData(clums,str);
        }
    }

    private String getColums(List<Map<String, Object>> data) {
        String[] keys = null;
        if (data.size() > 0) {
            Map<String,Object> map = data.get(0);
            keys = new String[map.size()];
            int i = 0;
            for (Map.Entry<String,Object> entry : map.entrySet()) {
                keys[i] = entry.getKey();
                i++;
            }
            String clums = "";
            for (int j = 0; j < keys.length; j++) {
                clums = clums + "`" +keys[j].toString() + "`" + " varchar(99),";
            }
            clums = clums.substring(0, clums.length() - 1);
            return clums;
        } else {
            throw new RuntimeException("表数据为空");
        }

    }

    public List<String> getData(List<Map<String, Object>> data) {
        List<String> dataList = new ArrayList<String>();
        for (Map<String,Object> map : data) {
            String[] dataOne = new String[map.size()];
            int i = 0;
            for (Map.Entry<String,Object> entry : map.entrySet()) {
                dataOne[i] = entry.getValue().toString();
                i ++;
            }
            String dataFinal = "";
            for (int j = 0; j < dataOne.length; j++) {

                dataFinal = dataFinal + "'" + dataOne[j] + "'" + ",";
            }
            dataFinal = dataFinal.substring(0, dataFinal.length() - 1);
            dataList.add(dataFinal);
        }
        return dataList;
    }

    private String getcl(List<Map<String, Object>> data) {
        String[] keys = null;
        Map<String,Object> map = data.get(0);
        keys = new String[map.size()];
        int i = 0;
        for (Map.Entry<String,Object> entry : map.entrySet()) {
            keys[i] = entry.getKey();
            i++;
        }
        String clums = "";
        for (int j = 0; j < keys.length; j++) {
            clums = clums + "`" +keys[j].toString() + "`,";
        }
        clums = clums.substring(0, clums.length() - 1);
        return clums;
    }
}
