package com.genghis.ptas.charts.scatter.service.impl;

import com.genghis.ptas.apriori.Apriori;
import com.genghis.ptas.charts.scatter.dao.PictureDao;
import com.genghis.ptas.charts.scatter.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-26
 * Time: 下午6:37
 * To change this template use File | Settings | File Templates.
 */
@Service("pictureService")
public class PictureServiceImpl implements PictureService{

    @Autowired
    private PictureDao pictureDao;

    @Override
    public List<List<String>> getApriori() {
        Instances instances = pictureDao.getInstances();
        Apriori apriori = new Apriori();
        try{
            String[] options = new String[17];
            options[0] =  "-I";
            options[1] =  "-N";
            options[2] =  "10";         //关联结果数量
            options[3] =  "-T";
            options[4] =  "0";
            options[5] =  "-C";         //最小置信度参数
            options[6] =  "0.8";        //修改这个数据置信度
            options[7] =  "-D";         //递减迭代值
            options[8] =  "0.05";       //修改这个参数设置
            options[9] =  "-U";         //最小支持度上界参数
            options[10] =  "1.0";       //修改这个数据修改支持度
            options[11] =  "-M";        //最小支持度下界参数
            options[12] =  "0.2";       //修改这个数据修改支持度
            options[13] =  "-S";
            options[14] =  "-1.0";
            options[15] =  "-c";
            options[16] =  "-1";
            apriori.setOptions(options);
            apriori.buildAssociations(instances);
            return getList(apriori.getResultMap());
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    private List<List<String>> getList(Map<String, String> map) {
        List<List<String>> reList = new ArrayList<List<String>>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            List<String> list = new ArrayList<String>();
            list.add(entry.getKey().substring(1,entry.getKey().length()));
            list.add(entry.getValue());
            reList.add(list);
        }
        return reList;
    }

    @Override
    public String getJ48(){
        Instances instances = pictureDao.getInstances();
        try {
            instances.setClassIndex(instances.numAttributes() - 1);
            Classifier cfs = (Classifier) Class.forName("weka.classifiers.trees.J48").newInstance();
            cfs.buildClassifier(instances);
            Instance testInst;
            Evaluation testingEvaluation = new Evaluation(instances);
            int length = instances.numInstances();
            for (int i = 0; i < length; i++) {
                testInst = instances.instance(i);
                testingEvaluation.evaluateModelOnceAndRecordPrediction(cfs, testInst);
            }
            String result = cfs.toString();
            System.out.println(result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
            return null;
    }

    @Override
    public Instances getInstances() {
        return pictureDao.getInstances();
    }

}
