/**
 * Created with IntelliJ IDEA.
 * User: panzhuowen
 * Date: 14-7-19
 * Time: 上午7:58
 * To change this template use File | Settings | File Templates.
 */
package com.genghis.ptas.util;

import org.apache.commons.beanutils.BeanUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @version 1.0.1
 * @Author panzhuowen
 */
public class JDBCUtilDao {

    public List<Map<String,Object>> queryForList(Connection connection,String tableName) {
        List<Map<String, Object>> values = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String sql = "SELECT * FROM " + tableName;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            values = handleResultSetToMapList(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCTools.release(connection, preparedStatement, resultSet);
        }

        return values;
    }

    public List<Map<String, Object>> handleResultSetToMapList(ResultSet resultSet) {
        List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
        List<String> columnLabels = getColumnLabels(resultSet);
        Map<String, Object> map = null;
        try {
            while (resultSet.next()) {
                map = new HashMap<String, Object>();
                for (String columnLabel : columnLabels) {
                    Object value = resultSet.getObject(columnLabel);
                    map.put(columnLabel, value);
                }
                values.add(map);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return values;
    }

    private List<String> getColumnLabels(ResultSet resultSet) {
        List<String> labels = new ArrayList<String>();
        try {
            ResultSetMetaData rsmd = resultSet.getMetaData();
            for (int i = 0; i < rsmd.getColumnCount(); i++) {
                labels.add(rsmd.getColumnLabel(i + 1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }

    public List<String> getAllDataBase(Connection connection) {
        List<String> databases = new ArrayList<String>();
        ResultSet resultSet = null;
        try {
            DatabaseMetaData data = connection.getMetaData();
            resultSet = data.getCatalogs();
            while (resultSet.next()) {
                databases.add(resultSet.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCTools.release(connection,null,resultSet);
        }
        return databases;
    }

    public List<String> getAllTables(Connection connection) {
        List<String> tables = new ArrayList<String>();
        ResultSet resultSet = null;
        try {
            DatabaseMetaData data = connection.getMetaData();
            resultSet = data.getTables(null,null,null,new String[] {"TABLE"});
            while (resultSet.next()) {
                tables.add(resultSet.getString("TABLE_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCTools.release(connection,null,resultSet);
        }
        return tables;
    }
}
