/*
* FileLoaderService.java
* Created on  2014-5-8 下午7:51
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-8       panzhuowen    初始版本
*
*/
package com.genghis.ptas.loader.service;

import org.xml.sax.SAXException;
import weka.core.Instances;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface FileLoaderService {

    public String upFile(HttpServletRequest request, String upFile, String fileName) throws ServletException, IOException, ParserConfigurationException, SAXException,
            XPathExpressionException;

    public Instances fileLoader(String filePath) throws Exception;

    public List<String> instance2attribute(Instances ins);

    public List<String> instance2data(Instances ins);

    void instance2db(Instances ins);

    public List<Map<String,Object>> getMap();

    Instances newInstances(List<Map<String,Object>> list);

    Instances getInstances(Instances instances,List<Map<String,Object>> list);

    Instances queryInstances();

    void insertInstance(Instances instances,String tableName);

    public String listToString(List<String> list);

    public String changListToDataSet(List<String> list);

}
