/*
* TreeService.java
* Created on  2014-6-8 下午7:49
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-6-8       panzhuowen    初始版本
*
*/
package com.genghis.ptas.data.service;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public interface TreeService {

    public String getNeedString();

    public List<Map<String, String>> getResultMap(String result);

}
