/*
* FileLoaderDaoImpl.java
* Created on  2014-5-8 下午7:53
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-8       panzhuowen    初始版本
*
*/
package com.genghis.ptas.loader.dao.impl;

import com.genghis.core.weka.WekaBaseDao;
import com.genghis.core.weka.WekaFactory;
import com.genghis.ptas.loader.dao.FileLoaderDao;
import com.genghis.ptas.util.FileUp;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import weka.core.Instances;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
@Service("fileLoaderDao")
public class FileLoaderDaoImpl extends WekaFactory implements FileLoaderDao {

    @Override
    public String upFile(HttpServletRequest request, String upFile, String fileName) throws ServletException, IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        FileUp fileUp = new FileUp();
        Date date = new Date();
        String finalPath = date.getTime() + "";
        return fileUp.upData(request, finalPath, upFile, fileName);
    }

    @Override
    public void createTable(String clums, String tableName) {
        String drop = "DROP TABLE IF EXISTS data_temp";
        getJdbcTemplate().update(drop);
        String sql = "CREATE TABLE " + tableName + "(" + clums + ")";
        getJdbcTemplate().update(sql);
    }

    @Override
    public void insertData(String tableName, String data, String clums) {
        String sql = "INSERT INTO " + tableName + " " + "(" + clums + ")" + " VALUES(" + data + ")";
        getJdbcTemplate().update(sql);
    }

    @Override
    public List<Map<String, Object>> getMap() {
        String sql = "SELECT kmeans.data1,kmeans.data2 FROM kmeans";
        return getJdbcTemplate().queryForList(sql);
    }

    @Override
    public Instances queryInstances() {
        String sql = "SELECT * FROM data_temp";
        return getInstancesWithMySql(sql);
    }

    @Override
    public void insertInstance(Instances instances, String tableName) {
//        insertInstancesToDB(instances,tableName);
    }


}
