package com.genghis.ptas.charts.scatter.dao.impl;

import com.genghis.core.weka.WekaBaseDao;
import com.genghis.ptas.charts.scatter.dao.ScatterDao;
import com.genghis.ptas.charts.scatter.entity.KMeansTestVo;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Surc
 * Date: 14-4-15
 * Time: 下午5:33
 * To change this template use File | Settings | File Templates.
 */
@Repository("scatterDao")
public class ScatterDaoImpl extends WekaBaseDao implements ScatterDao {

    public Object[] getKMeansDataFromDb(String clm) {
        String sql = "SELECT " + clm + " FROM data_temp";
        List<KMeansTestVo> dataList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<KMeansTestVo>(KMeansTestVo.class));
        Object[] dataArr = dataList.toArray();
        return dataArr;
    }

    @Override
    public List<Map<String, Object>> getData() {
        String sql = "SELECT * FROM data_temp";
        return getJdbcTemplate().queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> getFinalData(String clums) {
        String sql = "SELECT " + clums + " FROM data_temp";
        return getJdbcTemplate().queryForList(sql);
    }

    @Override
    public Instances getInstances() {
        String sql = "SELECT * FROM data_temp";
        return queryForInstancesWithMySQL(sql);
    }
}
