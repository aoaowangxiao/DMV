package com.genghis.ptas.clean.dao;

import weka.core.Instances;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: wangxiao
 * Date: 14-5-25
 * Time: 下午6:54
 * To change this template use File | Settings | File Templates.
 */
public interface DataCleanDao {

    public List<Map<String,Object>> db2MapList();

    Instances getInstances();

    void insertDataSet(String data,String clums);

    void cleanTable();

    void dropClum(String clName);
}
