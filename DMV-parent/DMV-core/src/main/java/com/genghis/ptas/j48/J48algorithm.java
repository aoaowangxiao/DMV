package com.genghis.ptas.j48;
/**
 * Created with IntelliJ IDEA.
 * User: Peter
 * Date: 14-5-12
 * Time: 下午7:57
 * To change this template use File | Settings | File Templates.
 */

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.File;

public class J48algorithm {

    public Classifier getCfs(Instances ins) {
        try {
            ins.setClassIndex(ins.numAttributes() - 1);
            Classifier cfs = (Classifier) Class.forName("weka.classifiers.trees.J48").newInstance();
            cfs.buildClassifier(ins);
            Instance testInst;
            Evaluation testingEvaluation = new Evaluation(ins);
            int length = ins.numInstances();
            for (int i = 0; i < length; i++) {
                testInst = ins.instance(i);
                testingEvaluation.evaluateModelOnceAndRecordPrediction(cfs, testInst);
            }
            return cfs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}