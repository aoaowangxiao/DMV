/*
* WekafactoryImpl.java
* Created on  2014-5-20 下午4:15
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-20       panzhuowen    初始版本
*
*/
package com.genghis.core.weka;

import com.genghis.core.dao.BaseDao;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.experiment.InstanceQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public class WekaFactory extends BaseDao {

    public <T extends Instances> T getInstances(String filePath) {
        File file = new File(filePath);
        return getInstances(file);
    }

    public <T extends Instances> T getInstancesWithMySql(String sql) {
        try {
            InstanceQuery query = new InstanceQuery();
            query.setDatabaseURL("jdbc:mysql://localhost:3306/dmv");
            query.setUsername("root");
            query.setPassword("root");
            query.setQuery(sql);
            return (T) query.retrieveInstances();
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public <T extends Instances> T getInstances(File file) {
        Instances ins = null;
        try {
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setFile(file);
            ins = arffLoader.getDataSet();
        } catch (Exception e) {
            //to catch Exception
        }
        return (T) ins;
    }

    public List<String> instance2attribute(Instances ins) {
        int n = ins.numAttributes();
        Attribute[] attributes = new Attribute[n];
        List<String> att = new ArrayList<String>();
        for (int i = 0; i < n; i++) {
            attributes[i] = ins.attribute(i);
            att.add(attributes[i].name());
        }
        return att;
    }

    public List<String> instance2data(Instances ins) {
        int n = ins.numInstances();
        List<String> data = new ArrayList<String>();
        for (int i = 0; i < n; i++) {
            data.add(ins.instance(i).toString());
        }
        return data;
    }

    public String getColumns(List<String> list) {
        String columns = "";
        for (String str : list) {
            columns = columns + "{ \"title\": \"" + str + "\" },";
        }
        columns = columns.substring(0, columns.length() - 1);
        columns = "[" + columns + "]";
        return columns;
    }

    public String getDataSet(List<String> list) {
        String data = "";
        for (String str : list) {
            String[] strings;
            String strTemp = "";
            strings = str.split(",");
            for (int i = 0; i < strings.length; i++) {
                strTemp = strTemp + "'" + strings[i] + "',";
            }
            strTemp = strTemp.substring(0, strTemp.length() - 1);
            data = data + "[" + strTemp + "],";
        }
        data = data.substring(0, data.length() - 1);
        data = "[" + data + "]";
        return data;
    }

    public int[] getIndexForNotNum(List<String> attrs) {
        int[] indexs = new int[attrs.size()];
        for (int i = 0; i < indexs.length; i++) {
            indexs[i] = -1;
        }
        int index = 0;
        for (int i = 0; i < attrs.size(); i++) {
            try {
                int a = Integer.parseInt(attrs.get(i));
            } catch (Exception e) {
                indexs[index] = i;
                index++;
            }
        }
        return indexs;
    }

    public List<String> rebuildData(List<String> data, int[] indexs) {
        List<String> newData = new ArrayList<String>();
        for (int i = 0; i < indexs.length; i++) {
            String[] strings;
            String[] newStrings = {};
            if (indexs[i] != -1) {
                for (String str : data) {
                    strings = str.split(",");
                    newStrings = removeAt(strings, indexs[i]);
                }
                newData.add(toString(newStrings));
            }
        }
        return newData;
    }

    public void rebuildAttr(List<String> attr, int[] indexs) {
        for (int i = 0; i < indexs.length; i++) {
            if (indexs[i] != -1) {
                attr.remove(indexs[i]);
            }
        }
    }

    public String toString(String[] strings) {
        String str = "";
        for (int i = 0; i < strings.length; i++) {
            str = str + strings[i] + ",";
        }
        str = str.substring(0, str.length() - 1);
        return str;
    }

    public String[] removeAt(String[] strings, int index) {
        for (int i = index; i < strings.length - 1; i++) {
            strings[i] = strings[i + 1];
        }
        String[] newStr = new String[strings.length - 1];
        System.arraycopy(strings, 0, newStr, 0, newStr.length);
        return newStr;
    }
}
