/*
* WekaBaseDao.java
* Created on  2014-5-17 下午2:56
* 版本       修改时间          作者      修改内容
* V1.0.1    2014-5-17       panzhuowen    初始版本
*
*/
package com.genghis.core.weka;

import com.genghis.core.dao.BaseDao;
import weka.core.Instances;
import weka.core.converters.DatabaseSaver;
import weka.experiment.InstanceQuery;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public class WekaBaseDao extends BaseDao {

    public Instances queryForInstancesWithMySQL(String sql) {
        try {
            InstanceQuery query = new InstanceQuery();
            query.setDatabaseURL("jdbc:mysql://localhost:3306/dmv");
            query.setUsername("root");
            query.setPassword("root");
            query.setQuery(sql);
            return query.retrieveInstances();
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public void insertInstancesToDB(Instances instances, String tableName) {
        try {
            instances.setClassIndex(instances.numAttributes() - 1);
            DatabaseSaver saver = new DatabaseSaver();
            saver.setUrl("jdbc:mysql://localhost:3306/dmv");
            saver.setUser("root");
            saver.setPassword("root");
            saver.setInstances(instances);
            saver.setRelationForTableName(false);
            saver.setTableName(tableName);
            saver.connectToDatabase();
            saver.writeBatch();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
